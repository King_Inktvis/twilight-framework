mod context;

use lazy_static::lazy_static;
use std::sync::{Arc, Mutex};
use twilight_framework::app::{AppBuilder, AppService};
use twilight_framework::responses::InteractiveResponse;
use twilight_framework::traits::Responder;
use twilight_model::application::interaction::message_component::MessageComponentInteractionData;
use twilight_model::application::interaction::{Interaction, InteractionData, InteractionType};
use twilight_model::channel::message::component::ComponentType;
use twilight_model::gateway::event::Event;
use twilight_model::gateway::payload::incoming::InteractionCreate;
use twilight_model::id::Id;
use twilight_util::builder::InteractionResponseDataBuilder;

lazy_static! {
    pub static ref CHECK_INTERACTIVE_RESPONSE: Mutex<bool> = Mutex::new(false);
}

async fn handle_check_interactive_response(_state: ()) {
    let mut lock = CHECK_INTERACTIVE_RESPONSE.lock().unwrap();
    *lock = true;
}

#[tokio::test]
async fn test_interactive_response() {
    let service = Arc::new(
        AppBuilder::new("token".to_string(), 1234)
            .build_no_registration()
            .await
            .unwrap(),
    );
    let data = InteractionResponseDataBuilder::new()
        .content("content".to_string())
        .build();
    let resp = InteractiveResponse::new((), handle_check_interactive_response, data);
    let uuid = resp.uuid();
    resp.responder(
        &service.ctx,
        &Interaction {
            app_permissions: None,
            application_id: Id::new(123),
            channel_id: None,
            channel: None,
            data: None,
            guild_id: None,
            guild_locale: None,
            id: Id::new(123),
            kind: InteractionType::ApplicationCommand,
            locale: None,
            member: None,
            message: None,
            token: "".to_string(),
            user: None,
        },
    )
    .await;
    AppService::handle(
        service,
        Event::InteractionCreate(Box::new(InteractionCreate(Interaction {
            app_permissions: None,
            application_id: Id::new(123),
            channel_id: None,
            channel: None,
            data: Some(InteractionData::MessageComponent(Box::from(
                MessageComponentInteractionData {
                    custom_id: uuid.to_string(),
                    component_type: ComponentType::ActionRow,
                    resolved: None,
                    values: vec![],
                },
            ))),
            guild_id: None,
            guild_locale: None,
            id: Id::new(123),
            kind: InteractionType::MessageComponent,
            locale: None,
            member: None,
            message: None,
            token: "".to_string(),
            user: None,
        }))),
    )
    .await;
    let lock = CHECK_INTERACTIVE_RESPONSE.lock().unwrap();
    assert!(*lock);
}
