use lazy_static::lazy_static;
use std::sync::Mutex;
use twilight_framework::app::AppBuilder;
use twilight_model::application::interaction::{Interaction, InteractionType};
use twilight_model::id::Id;
use uuid::Uuid;

lazy_static! {
    pub static ref CHECK_REGISTERED_COMPONENT: Mutex<bool> = Mutex::new(false);
}

#[tokio::test]
async fn test_call_registered_component_function() {
    let service = AppBuilder::new("token".to_string(), 1234)
        .build_no_registration()
        .await
        .unwrap();
    let uuid = Uuid::new_v4();
    service
        .ctx
        .register_component_handle(uuid, (), handle_check_registered_component_check)
        .await;
    let func = service.get_component(&uuid).await.unwrap();
    func.call(
        &service.ctx,
        Interaction {
            app_permissions: None,
            application_id: Id::new(123),
            channel_id: None,
            channel: None,
            data: None,
            guild_id: None,
            guild_locale: None,
            id: Id::new(123),
            kind: InteractionType::MessageComponent,
            locale: None,
            member: None,
            message: None,
            token: "".to_string(),
            user: None,
        },
    )
    .await;
    let check = CHECK_REGISTERED_COMPONENT.lock().unwrap();
    assert!(*check);
}

async fn handle_check_registered_component_check(_state: ()) {
    let mut lock = CHECK_REGISTERED_COMPONENT.lock().unwrap();
    *lock = true;
}
