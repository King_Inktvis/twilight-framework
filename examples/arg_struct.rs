use std::env;
use std::error::Error;
use twilight_framework::app::AppBuilder;
use twilight_framework::events::interaction_create::macros::{ArgType, GenArgs};
use twilight_framework::events::interaction_create::CommandBuilder;
use twilight_gateway::{EventTypeFlags, Shard};
use twilight_model::gateway::{Intents, ShardId};

#[derive(GenArgs)]
struct Args {
    #[description = "Language to greet in"]
    language: Language,
    #[description = "Name of whoever to greet"]
    name: Option<String>,
}

#[derive(ArgType)]
enum Language {
    English,
    French,
}

async fn greet(arg: Args) -> String {
    let name = match arg.name {
        Some(name) => name,
        None => String::new(),
    };
    match arg.language {
        Language::English => format!("Hello {}", name),
        Language::French => format!("Bonjour {}", name),
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    dotenv::dotenv().ok();
    let token = env::var("TOKEN").unwrap();
    let application_id = env::var("APPLICATION_ID").unwrap().parse::<u64>().unwrap();

    let intents = Intents::GUILD_MESSAGES | Intents::DIRECT_MESSAGES;
    let shard = Shard::new(ShardId::ONE, token.clone(), intents);
    let mut builder = AppBuilder::new(token, application_id);
    builder.add_interaction(
        CommandBuilder::new("greet", "Greet something")
            .handler(greet)
            .set_options::<Args>(),
    );

    let app = builder.build().await?;
    app.serve(shard, EventTypeFlags::all()).await;
    Ok(())
}
