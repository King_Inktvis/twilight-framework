use std::env;
use std::error::Error;
use twilight_framework::app::AppBuilder;
use twilight_gateway::{EventTypeFlags, Shard};
use twilight_model::gateway::payload::incoming::MessageCreate;
use twilight_model::gateway::{Intents, ShardId};

async fn print_message_content(message: MessageCreate) {
    println!("{}", message.content);
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    dotenv::dotenv().ok();
    let token = env::var("TOKEN").unwrap();
    let application_id = env::var("APPLICATION_ID").unwrap().parse::<u64>().unwrap();

    let intents = Intents::GUILD_MESSAGES | Intents::DIRECT_MESSAGES | Intents::MESSAGE_CONTENT;
    let shard = Shard::new(ShardId::ONE, token.clone(), intents);

    let mut builder = AppBuilder::new(token, application_id);
    builder.message_create(print_message_content);

    let app = builder.build().await?;
    app.serve(shard, EventTypeFlags::all()).await;
    Ok(())
}
