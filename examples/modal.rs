use std::env;
use std::error::Error;
use std::sync::Arc;
use twilight_framework::app::AppBuilder;
use twilight_framework::events::interaction_create::macros::GenArgs;
use twilight_framework::events::interaction_create::CommandBuilder;
use twilight_framework::responses::ModalResponse;
use twilight_gateway::{EventTypeFlags, Shard};
use twilight_model::application::interaction::modal::ModalInteractionData;
use twilight_model::channel::message::component::{ActionRow, TextInput, TextInputStyle};
use twilight_model::channel::message::{Component, Embed};
use twilight_model::gateway::{Intents, ShardId};
use twilight_model::http::interaction::InteractionResponseData;

struct ModalState {
    title: String,
}

fn get_value(name: &str, data: ModalInteractionData) -> Option<String> {
    for component in data.components {
        for inner_component in component.components {
            if inner_component.custom_id == name {
                return inner_component.value;
            }
        }
    }
    None
}

async fn modal_handle(state: Arc<ModalState>, data: ModalInteractionData) -> Embed {
    let body = get_value("body", data).unwrap();
    Embed {
        author: None,
        color: None,
        description: Some(body),
        fields: vec![],
        footer: None,
        image: None,
        kind: "".to_string(),
        provider: None,
        thumbnail: None,
        timestamp: None,
        title: Some(state.title.clone()),
        url: None,
        video: None,
    }
}

#[derive(GenArgs)]
struct Arg {
    #[description = "Title of the post"]
    title: String,
}

async fn create_post(arg: Arg) -> ModalResponse {
    let state = Arc::new(ModalState { title: arg.title });
    let tmp = TextInput {
        custom_id: "body".to_string(),
        label: "Body".to_string(),
        max_length: None,
        min_length: None,
        placeholder: None,
        required: None,
        style: TextInputStyle::Paragraph,
        value: None,
    };
    let row = vec![Component::ActionRow(ActionRow {
        components: vec![Component::TextInput(tmp)],
    })];

    let data = InteractionResponseData {
        allowed_mentions: None,
        attachments: None,
        choices: None,
        components: Some(row),
        content: None,
        custom_id: Some("".into()),
        embeds: None,
        flags: None,
        title: Some("Post Body".into()),
        tts: None,
    };
    ModalResponse::new(state, modal_handle, data)
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    dotenv::dotenv().ok();
    let token = env::var("TOKEN").unwrap();
    let application_id = env::var("APPLICATION_ID").unwrap().parse::<u64>().unwrap();

    let intents = Intents::GUILD_MESSAGES | Intents::DIRECT_MESSAGES;
    let shard = Shard::new(ShardId::ONE, token.clone(), intents);

    let mut builder = AppBuilder::new(token, application_id);
    builder.add_interaction(
        CommandBuilder::new("post", "Create a post")
            .handler(create_post)
            .set_options::<Arg>(),
    );

    let app = builder.build().await?;
    app.serve(shard, EventTypeFlags::all()).await;
    Ok(())
}
