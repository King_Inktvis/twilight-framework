use std::env;
use std::error::Error;
use twilight_framework::app::AppBuilder;
use twilight_framework::events::interaction_create::macros::GenArgs;
use twilight_framework::events::interaction_create::CommandBuilder;
use twilight_gateway::{EventTypeFlags, Shard};
use twilight_model::application::command::{CommandOption, CommandOptionType};
use twilight_model::gateway::{Intents, ShardId};

#[derive(GenArgs)]
struct Args {
    name: String,
}

async fn greet(arg: Args) -> String {
    format!("Hello {}", arg.name)
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    dotenv::dotenv().ok();
    let token = env::var("TOKEN").unwrap();
    let application_id = env::var("APPLICATION_ID").unwrap().parse::<u64>().unwrap();

    let intents = Intents::GUILD_MESSAGES | Intents::DIRECT_MESSAGES | Intents::MESSAGE_CONTENT;
    let shard = Shard::new(ShardId::ONE, token.clone(), intents);

    let option = CommandOption {
        autocomplete: Some(false),
        channel_types: None,
        choices: None,
        description: "Name of whoever to greet".into(),
        name: "name".into(),
        required: Some(true),
        description_localizations: None,
        kind: CommandOptionType::String,
        max_length: None,
        name_localizations: None,
        min_length: None,
        max_value: None,
        min_value: None,
        options: None,
    };

    let mut builder = AppBuilder::new(token, application_id);
    builder.add_interaction(
        CommandBuilder::new("greet", "Greet something")
            .handler(greet)
            .option(option),
    );

    let app = builder.build().await?;
    app.serve(shard, EventTypeFlags::all()).await;
    Ok(())
}
