use std::env;
use std::error::Error;
use std::sync::Arc;
use tokio::sync::Mutex;
use twilight_framework::app::AppBuilder;
use twilight_framework::events::interaction_create::message_component::{
    ComponentName, ResponseComponent,
};
use twilight_framework::events::interaction_create::CommandBuilder;
use twilight_framework::responses::EditMessage;
use twilight_gateway::{EventTypeFlags, Shard};
use twilight_model::channel::message::component::{ActionRow, Button, ButtonStyle};
use twilight_model::channel::message::Component;
use twilight_model::gateway::{Intents, ShardId};
use twilight_util::builder::InteractionResponseDataBuilder;

#[derive(Default, Clone)]
struct Counter(Arc<Mutex<i32>>);

impl Counter {
    async fn increment(&self) -> i32 {
        let mut lock = self.0.lock().await;
        *lock += 1;
        *lock
    }

    async fn decrement(&self) -> i32 {
        let mut lock = self.0.lock().await;
        *lock -= 1;
        *lock
    }
}

async fn create_counter() -> ResponseComponent {
    let row = [Component::ActionRow(ActionRow {
        components: vec![
            Component::Button(Button {
                custom_id: Some("-".to_string()),
                disabled: false,
                emoji: None,
                label: Some("-".to_string()),
                style: ButtonStyle::Primary,
                url: None,
            }),
            Component::Button(Button {
                custom_id: Some("+".to_string()),
                disabled: false,
                emoji: None,
                label: Some("+".to_string()),
                style: ButtonStyle::Primary,
                url: None,
            }),
        ],
    })];

    let data = InteractionResponseDataBuilder::new()
        .content("0")
        .components(row)
        .build();

    ResponseComponent::new(data, Counter::default(), click_handle)
}

async fn click_handle(counter: Counter, id_attachment: ComponentName) -> Result<EditMessage, ()> {
    let new_value = match id_attachment.0.as_str() {
        "+" => counter.increment().await,
        "-" => counter.decrement().await,
        _ => return Err(()),
    };
    let data = InteractionResponseDataBuilder::new()
        .content(new_value.to_string())
        .build();
    Ok(data.into())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    dotenv::dotenv().ok();
    let token = env::var("TOKEN").unwrap();
    let application_id = env::var("APPLICATION_ID").unwrap().parse::<u64>().unwrap();

    let intents = Intents::GUILD_MESSAGES | Intents::DIRECT_MESSAGES;
    let shard = Shard::new(ShardId::ONE, token.clone(), intents);

    let mut builder = AppBuilder::new(token, application_id);
    builder.add_interaction(
        CommandBuilder::new("counter", "Create a counter").handler(create_counter),
    );

    let app = builder.build().await?;
    app.serve(shard, EventTypeFlags::all()).await;
    Ok(())
}
