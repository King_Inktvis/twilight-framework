use std::env;
use std::error::Error;
use twilight_framework::app::AppBuilder;
use twilight_framework::events::interaction_create::{macros::GenArgs, CommandBuilder, SubCommand};
use twilight_gateway::{EventTypeFlags, Shard};
use twilight_model::application::command::{CommandOptionChoice, CommandOptionChoiceValue};
use twilight_model::application::interaction::application_command::{
    CommandData, CommandOptionValue,
};
use twilight_model::gateway::{Intents, ShardId};
use twilight_model::http::interaction::{InteractionResponse, InteractionResponseType};
use twilight_util::builder::InteractionResponseDataBuilder;

#[derive(GenArgs)]
struct Arg {
    #[description = "Name of whoever to greet"]
    #[autocomplete = true]
    name: String,
}

async fn favorite_f1(arg: Arg) -> String {
    format!("Your favorite F1 driver is: {}", arg.name)
}

async fn autocomplete(app: CommandData) -> InteractionResponse {
    let mut matches: Vec<CommandOptionChoice> = LIST
        .iter()
        .filter_map(|driver| {
            if let CommandOptionValue::Focused(value, _) = &app.options[0].value {
                if driver.to_lowercase().contains(&value.to_lowercase()) {
                    return Some(CommandOptionChoice {
                        name: driver.to_string(),
                        name_localizations: None,
                        value: CommandOptionChoiceValue::String(driver.to_string()),
                    });
                }
            }
            None
        })
        .collect();
    matches.truncate(25);
    InteractionResponse {
        kind: InteractionResponseType::ApplicationCommandAutocompleteResult,
        data: Some(
            InteractionResponseDataBuilder::new()
                .choices(matches)
                .build(),
        ),
    }
}

static LIST: &[&str] = &[
    "Fernando Alonso",
    "Mario Andretti",
    "Alberto Ascari",
    "Jack Brabham",
    "Jenson Button",
    "Jim Clark",
    "Juan Manuel Fangio",
    "Giuseppe Farina",
    "Emerson Fittipaldi",
    "Mika Häkkinen",
    "Lewis Hamilton",
    "Damon Hill",
    "Graham Hill",
    "Phil Hill",
    "Denny Hulme",
    "James Hunt",
    "Alan Jones",
    "Niki Lauda",
    "Nigel Mansell",
    "Nelson Piquet",
    "Alain Prost",
    "Kimi Räikkönen",
    "Jochen Rindt",
    "Keke Rosberg",
    "Nico Rosberg",
    "Jody Scheckter",
    "Michael Schumacher",
    "Ayrton Senna",
    "Jackie Stewart",
    "John Surtees",
    "Max Verstappen",
    "Sebastian Vettel",
    "Jacques Villeneuve",
];

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    dotenv::dotenv().ok();
    let token = env::var("TOKEN").unwrap();
    let application_id = env::var("APPLICATION_ID").unwrap().parse::<u64>().unwrap();

    let intents = Intents::GUILD_MESSAGES | Intents::DIRECT_MESSAGES | Intents::MESSAGE_CONTENT;
    let shard = Shard::new(ShardId::ONE, token.clone(), intents);

    let mut builder = AppBuilder::new(token, application_id);
    builder.add_interaction(
        CommandBuilder::new("favorite", "Call your favorite").sub_command(
            SubCommand::new("f1", "Favorite f1 driver", favorite_f1)
                .set_options::<Arg>()
                .autocomplete(autocomplete),
        ),
    );

    let app = builder.build().await;
    match app {
        Ok(app) => {
            app.serve(shard, EventTypeFlags::all()).await;
        }
        Err(e) => {
            println!("{}", e);
        }
    }

    Ok(())
}
