use super::traits::Responder;
use crate::app::{component_uuid_apply, Context};
use crate::arguments::{Extract, StateExtract};
use crate::events::interaction_create::message_component::ComponentId;
use crate::traits::ContextResponder;
use crate::Handler;
use async_trait::async_trait;
use std::marker::PhantomData;
use twilight_model::application::interaction::Interaction;
use twilight_model::http::interaction::{
    InteractionResponse, InteractionResponseData, InteractionResponseType,
};
use twilight_model::id::marker::{InteractionMarker, UserMarker};
use twilight_model::id::Id;
use uuid::Uuid;

/// Send a string as response to a command.
/// This function fails silently.
pub async fn interaction_respond(
    ctx: &Context,
    id: Id<InteractionMarker>,
    token: &str,
    text: String,
) {
    let interaction_client = ctx.client.interaction(ctx.application_id);
    if let Err(e) = interaction_client
        .create_response(
            id,
            token,
            &InteractionResponse {
                kind: InteractionResponseType::ChannelMessageWithSource,
                data: Some(
                    twilight_util::builder::InteractionResponseDataBuilder::new()
                        .content(text)
                        .build(),
                ),
            },
        )
        .await
    {
        tracing::error!("{e}");
    }
}

#[async_trait]
impl<T, E, D> Responder<D> for Result<T, E>
where
    T: Responder<D>,
    E: Responder<D>,
    D: Send + Sync,
{
    async fn responder(self, ctx: &Context, result_context: &D) {
        match self {
            Ok(v) => Box::new(v).responder(ctx, result_context).await,
            Err(v) => Box::new(v).responder(ctx, result_context).await,
        };
    }
}

#[async_trait]
impl Responder<Interaction> for InteractionResponse {
    async fn responder(self, ctx: &Context, result_context: &Interaction) {
        if let Err(e) = ctx
            .client
            .interaction(ctx.application_id)
            .create_response(result_context.id, &result_context.token, &self)
            .await
        {
            tracing::error!("{e}");
        }
    }
}

/// A return type which will send the given component as reaction.
/// The component will be registered and further interactions will be handled by the provided function.
pub struct InteractiveResponse {
    inner: Box<dyn Register>,
    data: InteractionResponseData,
    uuid: Uuid,
}

impl InteractiveResponse {
    pub fn new<Func, Args, State>(state: State, handle: Func, data: InteractionResponseData) -> Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<Interaction>,
        Args: StateExtract<Interaction, State> + 'static,
        State: Send + Sync + 'static,
    {
        let uuid = Uuid::new_v4();
        Self {
            inner: Box::new(RegisterResponseInner {
                args: PhantomData,
                interaction: PhantomData,
                state,
                handle,
            }),
            data,
            uuid,
        }
    }

    #[must_use]
    pub fn uuid(&self) -> Uuid {
        self.uuid
    }
}

#[async_trait]
impl Responder<Interaction> for InteractiveResponse {
    async fn responder(mut self, ctx: &Context, result_context: &Interaction) {
        if let Some(ref mut data) = self.data.components {
            component_uuid_apply(&self.uuid, data.as_mut());
            // ctx.store_handler(self.uuid, self.handle).await;
        }
        self.inner.register(self.uuid, ctx).await;

        let response = InteractionResponse {
            kind: InteractionResponseType::ChannelMessageWithSource,
            data: Some(self.data),
        };
        send_response(
            ctx,
            result_context.id,
            result_context.token.as_str(),
            &response,
        )
        .await;
    }
}

#[async_trait]
trait Register: Send + Sync {
    async fn register(self: Box<Self>, uuid: Uuid, ctx: &Context);
}

pub struct RegisterResponseInner<State, Func, Args, Int>
where
    State: Send + Sync + 'static,
    Func: Handler<Args>,
    Func::Output: Responder<Int>,
    Args: StateExtract<Int, State> + 'static,
{
    args: PhantomData<Args>,
    interaction: PhantomData<Int>,
    state: State,
    handle: Func,
}

#[async_trait]
impl<State, Func, Args> Register for RegisterResponseInner<State, Func, Args, Interaction>
where
    State: Send + Sync + 'static,
    Func: Handler<Args>,
    Func::Output: Responder<Interaction>,
    Args: StateExtract<Interaction, State> + 'static,
{
    async fn register(self: Box<Self>, uuid: Uuid, ctx: &Context) {
        ctx.register_component_handle(uuid, self.state, self.handle)
            .await;
    }
}

pub struct ModalResponse {
    inner: Box<dyn Register>,
    fields: InteractionResponseData,
    uuid: Uuid,
}

impl ModalResponse {
    pub fn new<State, Func, Args>(
        state: State,
        handle: Func,
        fields: InteractionResponseData,
    ) -> Self
    where
        State: Send + Sync + 'static,
        Func: Handler<Args>,
        Func::Output: Responder<Interaction>,
        Args: StateExtract<Interaction, State> + 'static,
    {
        Self {
            inner: Box::new(RegisterResponseInner {
                args: PhantomData,
                interaction: PhantomData,
                state,
                handle,
            }),
            fields,
            uuid: Uuid::new_v4(),
        }
    }

    #[must_use]
    pub fn uuid(&self) -> Uuid {
        self.uuid
    }
}

#[async_trait]
impl Responder<Interaction> for ModalResponse {
    async fn responder(mut self, ctx: &Context, result_context: &Interaction) {
        self.fields.custom_id = Some(self.uuid.to_string());
        self.inner.register(self.uuid, ctx).await;

        let response = InteractionResponse {
            kind: InteractionResponseType::Modal,
            data: Some(self.fields),
        };
        send_response(
            ctx,
            result_context.id,
            result_context.token.as_str(),
            &response,
        )
        .await;
    }
}

/// A response type which updates the message is is responding to.
/// This is meant for use with components
#[derive(Debug)]
pub struct EditMessage(InteractionResponseData);

impl From<InteractionResponseData> for EditMessage {
    fn from(i: InteractionResponseData) -> Self {
        Self(i)
    }
}

#[async_trait]
impl Responder<Interaction> for EditMessage {
    async fn responder(mut self, ctx: &Context, result_context: &Interaction) {
        if let Some(components) = &mut self.0.components {
            if let Ok(id) = ComponentId::extract(ctx, result_context).await {
                component_uuid_apply(&id.0, components);
            }
        }
        let response = InteractionResponse {
            kind: InteractionResponseType::UpdateMessage,
            data: Some(self.0),
        };
        send_response(
            ctx,
            result_context.id,
            result_context.token.as_str(),
            &response,
        )
        .await;
    }
}

/// Send a response to a interaction.
pub(crate) async fn send_response(
    ctx: &Context,
    id: Id<InteractionMarker>,
    token: &str,
    response: &InteractionResponse,
) {
    let interaction = ctx.client.interaction(ctx.application_id);
    if let Err(e) = interaction.create_response(id, token, response).await {
        tracing::error!("{e}");
    }
}

#[async_trait]
impl<T, Data> Responder<Data> for Vec<T>
where
    T: Responder<Data>,
    Data: Send + Sync + 'static,
{
    async fn responder(self, ctx: &Context, result_context: &Data) {
        for r in self {
            Box::new(r).responder(ctx, result_context).await;
        }
    }
}

#[async_trait]
impl<T, Data> Responder<Data> for Option<T>
where
    T: Responder<Data>,
    Data: Send + Sync + 'static,
{
    async fn responder(self, ctx: &Context, result_context: &Data) {
        if let Some(inner) = self {
            Box::new(inner).responder(ctx, result_context).await;
        }
    }
}

/// A wrapper for a vector which takes multiple different response types.
#[derive(Default)]
pub struct ResponseBox<Data> {
    list: Vec<Box<dyn ResponseBoxInnerTrait<Data> + 'static>>,
}

impl<Data> ResponseBox<Data> {
    #[must_use]
    pub fn new() -> Self {
        Self { list: vec![] }
    }
}

#[async_trait]
pub(crate) trait ResponseBoxInnerTrait<Data>: Send + Sync {
    async fn respond_inner(self: Box<Self>, ctx: &Context, result_context: &Data);
}

pub(crate) struct ResponseBoxInner<T, Data>
where
    T: Responder<Data>,
{
    pub inner: Box<T>,
    pub data: PhantomData<Data>,
}

#[async_trait]
impl<T, Data> ResponseBoxInnerTrait<Data> for ResponseBoxInner<T, Data>
where
    T: Responder<Data>,
    Data: Send + Sync,
{
    async fn respond_inner(self: Box<Self>, ctx: &Context, result_context: &Data) {
        self.inner.responder(ctx, result_context).await;
    }
}

impl<Data> ResponseBox<Data> {
    pub fn add<T>(&mut self, response: T)
    where
        T: Responder<Data> + 'static,
        Data: Send + Sync + 'static,
    {
        self.list.push(Box::new(ResponseBoxInner {
            inner: Box::new(response),
            data: PhantomData,
        }));
    }
}

pub struct ErrorBox<Data> {
    pub(crate) inner: Box<dyn ResponseBoxInnerTrait<Data> + 'static>,
}

impl<Data> ErrorBox<Data>
where
    Data: Send + Sync + 'static,
{
    pub fn new<T: Responder<Data>>(value: T) -> Self {
        Self {
            inner: Box::new(ResponseBoxInner {
                inner: Box::new(value),
                data: PhantomData,
            }),
        }
    }
}

#[async_trait]
impl<Data> Responder<Data> for ErrorBox<Data>
where
    Data: Send + Sync + 'static,
{
    async fn responder(self, ctx: &Context, result_context: &Data) {
        self.inner.respond_inner(ctx, result_context).await;
    }
}

#[async_trait]
impl<Data> Responder<Data> for ResponseBox<Data>
where
    Data: Send + Sync + 'static,
{
    async fn responder(self, ctx: &Context, result_context: &Data) {
        for item in self.list {
            Box::new(item.respond_inner(ctx, result_context)).await;
        }
    }
}

pub struct DirectMessage {
    user: Id<UserMarker>,
    message: String,
}

#[async_trait]
impl ContextResponder for DirectMessage {
    async fn context_responder(self, ctx: &Context) {
        if let Err(e) = self.send_dm(ctx).await {
            tracing::error!("{e}");
        }
    }
}

impl DirectMessage {
    #[must_use]
    pub fn new(user: Id<UserMarker>, message: String) -> Self {
        Self { user, message }
    }

    async fn send_dm(self, ctx: &Context) -> Result<(), Box<dyn std::error::Error>> {
        let channel = ctx.client.create_private_channel(self.user).await?;
        let channel_id = channel.model().await?.id;
        let m = ctx.client.create_message(channel_id).content(&self.message);
        m.await?;
        Ok(())
    }
}

impl<T> From<T> for Box<dyn Responder<Interaction>>
where
    T: Responder<Interaction>,
{
    fn from(t: T) -> Self {
        Box::new(t)
    }
}
