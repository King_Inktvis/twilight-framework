use crate::app::Context;
use async_trait::async_trait;
use twilight_model::application::interaction::Interaction;
use twilight_model::gateway::payload::incoming::MessageCreate;

#[async_trait]
pub(crate) trait MessageBoxTrait: Send + Sync {
    async fn message_create(&self, ctx: &Context, msg: MessageCreate);
}

#[async_trait]
pub(crate) trait ComponentBoxTrait: Send + Sync {
    async fn component_interaction(
        &self,
        ctx: &Context,
        id: &str,
        id_attachment: &str,
        msg: Interaction,
    );
}

/// A trait that must be implemented by all handler return types.
/// In the responder trait are messages send to the Discord api
#[async_trait]
pub trait Responder<Data>: Send + Sync + 'static {
    async fn responder(self, ctx: &Context, data: &Data);
}

#[async_trait]
pub trait ContextResponder: Send + Sync + 'static {
    async fn context_responder(self, ctx: &Context);
}

#[async_trait]
impl<T, Data> Responder<Data> for T
where
    T: ContextResponder,
    Data: Send + Sync + 'static,
{
    async fn responder(self, ctx: &Context, _: &Data) {
        self.context_responder(ctx).await;
    }
}
