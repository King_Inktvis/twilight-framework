use crate::app::Context;
use crate::responses::{ErrorBox, ResponseBoxInner};
use crate::traits::Responder;
use crate::Call;
use async_trait::async_trait;
use std::future::Future;
use std::marker::PhantomData;
use twilight_model::application::interaction::Interaction;

pub trait Handler<Args>: Clone + 'static + Send + Sync {
    type Output;
    type Future: Future<Output = Self::Output> + Send;

    fn call(&self, args: Args) -> Self::Future;
}

/// The Extract<T> trait has to be implemented for every argument used in a handler.
/// The T is the type of the message discord uses for the type of event e.g. ApplicationCommand.
/// There are some implementations for types in the std and custom helper types.
/// If you need to get your own kind of data out of the message implement this trait for your own type.
#[async_trait]
pub trait Extract<T>: Sync + Send
where
    Self: Sized,
{
    type Error: Responder<T>;
    async fn extract(ctx: &Context, msg: &T) -> Result<Self, Self::Error>;
}

#[async_trait]
pub trait FromContext<T>: Sync + Send
where
    Self: Sized,
{
    type Error: Responder<T>;

    async fn from_context(ctx: &Context) -> Result<Self, Self::Error>;
}

#[async_trait]
impl<T> FromContext<T> for Context
where
    (): Responder<T>,
{
    type Error = ();

    async fn from_context(ctx: &Context) -> Result<Self, Self::Error> {
        Ok(ctx.clone())
    }
}

#[async_trait]
pub trait StateExtract<T, S>: Sync + Send
where
    Self: Sized,
{
    type Error: Responder<T>;

    async fn modal_extract(ctx: &Context, interaction: &T, state: &S) -> Result<Self, Self::Error>;
}

pub struct StateData<State, Func, Args, Int>
where
    State: Send + Sync,
    Func: Handler<Args>,
    Func::Output: Responder<Int>,
    Args: StateExtract<Int, State> + 'static,
{
    pub args: PhantomData<Args>,
    pub interaction: PhantomData<Int>,
    pub state: State,
    pub handle: Func,
}

impl<State, Func, Args, Int> StateData<State, Func, Args, Int>
where
    State: Send + Sync,
    Func: Handler<Args>,
    Func::Output: Responder<Int>,
    Args: StateExtract<Int, State> + 'static,
{
    pub fn new(state: State, handle: Func) -> Self {
        Self {
            args: PhantomData,
            interaction: PhantomData,
            state,
            handle,
        }
    }
}

#[async_trait]
impl<State, Func, Args> Call<Interaction> for StateData<State, Func, Args, Interaction>
where
    State: Send + Sync + 'static,
    Func: Handler<Args>,
    Func::Output: Responder<Interaction>,
    Args: StateExtract<Interaction, State> + 'static,
{
    async fn call(&self, ctx: &Context, interaction: Interaction) {
        match Args::modal_extract(ctx, &interaction, &self.state).await {
            Ok(extracted) => {
                let res = self.handle.call(extracted).await;
                res.responder(ctx, &interaction).await;
            }
            Err(e) => {
                e.responder(ctx, &interaction).await;
            }
        }
    }
}

#[allow(non_snake_case)]
pub(crate) mod macros {
    use super::*;

    macro_rules! handler_pattern ({ $($arg:ident)*} => {
        impl<Func, Fut,$($arg,)*> Handler<($($arg,)*)> for Func
        where
            Func: Fn($($arg,)*) -> Fut + Clone + 'static + Send + Sync,
            Fut: Future + Send,
        {
            type Output = Fut::Output;
            type Future = Fut;

            fn call(&self, ($($arg,)*): ($($arg,)*)) -> Self::Future {
                (self)($($arg,)*)
            }
        }
    });

    handler_pattern! {}
    handler_pattern! {A}
    handler_pattern! {A B}
    handler_pattern! {A B C}
    handler_pattern! {A B C D}
    handler_pattern! {A B C D E}
    handler_pattern! {A B C D E F}

    macro_rules! tuple_transform {( $($arg:ident)* ) => {
        #[async_trait]
        impl<T, $($arg,)*> Extract<T> for ($($arg,)*)
        where
            T: Send + Sync + 'static,
            $($arg: Extract<T> + 'static,)*
        {
            type Error = ErrorBox<T>;

            async fn extract(ctx: &Context, msg: &T) -> Result<Self,Self::Error> {
                Ok(
                    ($($arg::extract(ctx, msg).await.map_err(|e| ErrorBox {
                        inner:Box::new(
                            ResponseBoxInner {
                                inner:Box::new(e),
                                data: Default::default(),
                            }
                        )
                    })?,)*)
                )
            }
        }
    }}

    #[async_trait]
    impl<T> Extract<T> for ()
    where
        T: Send + Sync,
    {
        type Error = ();

        async fn extract(_ctx: &Context, _msg: &T) -> Result<Self, Self::Error> {
            Ok(())
        }
    }

    tuple_transform! {A}
    tuple_transform! {A B}
    tuple_transform! {A B C}
    tuple_transform! {A B C D}
    tuple_transform! {A B C D E}
    tuple_transform! {A B C D E F}

    macro_rules! impl_from_context {
        ( $arg:ident ) => {
            #[async_trait::async_trait]
            impl<C> crate::arguments::Extract<$arg> for C
            where
                C: crate::arguments::FromContext<$arg>,
            {
                type Error = C::Error;

                async fn extract(
                    ctx: &crate::app::Context,
                    _msg: &$arg,
                ) -> Result<Self, Self::Error> {
                    C::from_context(ctx).await
                }
            }
        };
    }

    macro_rules! impl_from_self {
        ( $arg:ident ) => {
            #[async_trait::async_trait]
            impl crate::arguments::Extract<$arg> for $arg {
                type Error = ();

                async fn extract(
                    _ctx: &crate::app::Context,
                    data: &$arg,
                ) -> Result<Self, Self::Error> {
                    Ok(data.clone())
                }
            }
        };
    }

    use crate::util::call_for_event_type;
    call_for_event_type!(impl_from_context);
    call_for_event_type!(impl_from_self);
}
