mod builder;
mod service;

use crate::arguments::{StateData, StateExtract};
use crate::traits::Responder;
use crate::{Call, Data, Handler};
pub use builder::AppBuilder;
pub use service::AppService;
use std::any::{Any, TypeId};
use std::collections::HashMap;
use std::sync::Arc;
use tokio::sync::RwLock;
use twilight_http::Client;
use twilight_model::application::interaction::Interaction;
use twilight_model::channel::message::Component;
use twilight_model::id::marker::ApplicationMarker;
use twilight_model::id::Id;
use uuid::Uuid;

type ComponentRegistry = Arc<RwLock<HashMap<Uuid, Arc<dyn Call<Interaction>>>>>;

/// Context of the application containing references for global information.
#[derive(Clone)]
#[must_use]
pub struct Context {
    /// Client for directly interaction with the Discord API
    pub client: Arc<Client>,
    /// Application ID as configured in the app builder.
    pub application_id: Id<ApplicationMarker>,
    data: Arc<HashMap<TypeId, Arc<dyn Any + Send + Sync + 'static>>>,
    component_registry: ComponentRegistry,
}

impl Context {
    /// Create a new application context object
    pub fn new(client: Client, application_id: Id<ApplicationMarker>) -> Self {
        Self {
            client: Arc::new(client),
            application_id,
            data: Arc::default(),
            component_registry: Arc::new(RwLock::default()),
        }
    }

    /// Register a function which will be called when the given component with the given uuid is interacted with.
    /// This function applies the uuid to the given component.
    pub async fn register_component<Func, Args, State>(
        &self,
        component: &mut [Component],
        state: State,
        handler: Func,
        uuid: Uuid,
    ) where
        State: Send + Sync + 'static,
        Func: Handler<Args>,
        Func::Output: Responder<Interaction>,
        Args: StateExtract<Interaction, State> + 'static,
    {
        component_uuid_apply(&uuid, component);
        self.register_component_handle(uuid, state, handler).await;
    }

    /// Add a function which will be called when a component has the given id as `custom_id`
    pub async fn register_component_handle<Func, Args, State>(
        &self,
        id: Uuid,
        state: State,
        handle: Func,
    ) where
        State: Send + Sync + 'static,
        Func: Handler<Args>,
        Func::Output: Responder<Interaction>,
        Args: StateExtract<Interaction, State> + 'static,
    {
        let msg_box = Arc::new(StateData::new(state, handle));
        let mut lock = self.component_registry.write().await;
        lock.insert(id, msg_box);
    }

    /// remove a stored handler by giving the id it operates on
    pub async fn component_entry(&self, id: &Uuid) {
        let mut lock = self.component_registry.write().await;
        lock.remove(id);
    }

    /// Fetch global data stored with data type T
    #[must_use]
    pub fn data<T>(&self) -> Option<Data<T>>
    where
        T: Send + Sync + 'static,
    {
        let tmp = self.data.get(&TypeId::of::<T>()).cloned();
        match tmp {
            Some(v) => match v.downcast::<T>() {
                Ok(v) => Some(Data(v)),
                Err(_) => None,
            },
            None => None,
        }
    }
}

/// Recursively apply the given uuid marker as prefix to all components custom ids
pub(crate) fn component_uuid_apply(uuid: &Uuid, list: &mut [Component]) {
    for i in list {
        match i {
            Component::ActionRow(action_row) => {
                component_uuid_apply(uuid, action_row.components.as_mut_slice());
            }
            Component::Button(b) => match &b.custom_id {
                None => b.custom_id = Some(uuid.to_string()),
                Some(custom_id) => b.custom_id = Some(format!("{uuid}{custom_id}")),
            },
            Component::SelectMenu(select_menu) => {
                select_menu.custom_id = format!("{uuid}{}", select_menu.custom_id);
            }
            Component::TextInput(text_input) => {
                text_input.custom_id = format!("{uuid}{}", text_input.custom_id);
            }
            Component::Unknown(u) => {
                tracing::error!("Unknown component type: {u}");
            }
        }
    }
}
