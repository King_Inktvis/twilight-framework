use crate::app::Context;
use crate::events::interaction_create::CommandNav;
use crate::events::Call;
use std::collections::HashMap;
use std::str::FromStr;
use std::sync::Arc;
use tracing::{error, Instrument};
use twilight_gateway::{EventTypeFlags, Shard, StreamExt};
use twilight_model::application::interaction::{Interaction, InteractionData, InteractionType};
use twilight_model::gateway::event::Event;
use twilight_model::gateway::payload::incoming::*;
use twilight_model::gateway::CloseFrame;
use uuid::Uuid;

/// Service object responsible for handling all incoming Discord events
pub struct AppService {
    pub(crate) commands: HashMap<String, CommandNav>,
    pub ctx: Context,
    pub(crate) message_component_fallback: Option<Box<dyn Call<Interaction>>>,
    pub(crate) modal_submit_fallback: Option<Box<dyn Call<Interaction>>>,
    pub(crate) auto_moderation_action_execution:
        Option<Box<dyn Call<AutoModerationActionExecution>>>,
    pub(crate) auto_moderation_rule_create: Option<Box<dyn Call<AutoModerationRuleCreate>>>,
    pub(crate) auto_moderation_rule_delete: Option<Box<dyn Call<AutoModerationRuleDelete>>>,
    pub(crate) auto_moderation_rule_update: Option<Box<dyn Call<AutoModerationRuleUpdate>>>,
    pub(crate) ban_add: Option<Box<dyn Call<BanAdd>>>,
    pub(crate) ban_remove: Option<Box<dyn Call<BanRemove>>>,
    pub(crate) channel_create: Option<Box<dyn Call<ChannelCreate>>>,
    pub(crate) channel_delete: Option<Box<dyn Call<ChannelDelete>>>,
    pub(crate) channel_pins_update: Option<Box<dyn Call<ChannelPinsUpdate>>>,
    pub(crate) channel_update: Option<Box<dyn Call<ChannelUpdate>>>,
    pub(crate) command_permissions_update: Option<Box<dyn Call<CommandPermissionsUpdate>>>,
    pub(crate) gateway_heartbeat: Option<Box<dyn Call<u64>>>,
    pub(crate) gateway_heartbeat_ack: Option<Box<dyn Call<()>>>,
    pub(crate) gateway_hello: Option<Box<dyn Call<Hello>>>,
    pub(crate) gateway_invalidate_session: Option<Box<dyn Call<bool>>>,
    pub(crate) gateway_reconnect: Option<Box<dyn Call<()>>>,
    pub(crate) guild_create: Option<Box<dyn Call<GuildCreate>>>,
    pub(crate) guild_delete: Option<Box<dyn Call<GuildDelete>>>,
    pub(crate) guild_emojis_update: Option<Box<dyn Call<GuildEmojisUpdate>>>,
    pub(crate) guild_integrations_update: Option<Box<dyn Call<GuildIntegrationsUpdate>>>,
    pub(crate) guild_scheduled_event_create: Option<Box<dyn Call<GuildScheduledEventCreate>>>,
    pub(crate) guild_scheduled_event_delete: Option<Box<dyn Call<GuildScheduledEventDelete>>>,
    pub(crate) guild_scheduled_event_update: Option<Box<dyn Call<GuildScheduledEventUpdate>>>,
    pub(crate) guild_scheduled_event_user_add: Option<Box<dyn Call<GuildScheduledEventUserAdd>>>,
    pub(crate) guild_scheduled_event_user_remove:
        Option<Box<dyn Call<GuildScheduledEventUserRemove>>>,
    pub(crate) guild_stickers_update: Option<Box<dyn Call<GuildStickersUpdate>>>,
    pub(crate) guild_update: Option<Box<dyn Call<GuildUpdate>>>,
    pub(crate) integration_create: Option<Box<dyn Call<IntegrationCreate>>>,
    pub(crate) integration_delete: Option<Box<dyn Call<IntegrationDelete>>>,
    pub(crate) integration_update: Option<Box<dyn Call<IntegrationUpdate>>>,
    pub(crate) invite_create: Option<Box<dyn Call<InviteCreate>>>,
    pub(crate) invite_delete: Option<Box<dyn Call<InviteDelete>>>,
    pub(crate) member_add: Option<Box<dyn Call<MemberAdd>>>,
    pub(crate) member_remove: Option<Box<dyn Call<MemberRemove>>>,
    pub(crate) member_update: Option<Box<dyn Call<MemberUpdate>>>,
    pub(crate) member_chunk: Option<Box<dyn Call<MemberChunk>>>,
    pub(crate) message_create: Option<Box<dyn Call<MessageCreate>>>,
    pub(crate) message_delete: Option<Box<dyn Call<MessageDelete>>>,
    pub(crate) message_delete_bulk: Option<Box<dyn Call<MessageDeleteBulk>>>,
    pub(crate) message_update: Option<Box<dyn Call<MessageUpdate>>>,
    pub(crate) presence_update: Option<Box<dyn Call<PresenceUpdate>>>,
    pub(crate) reaction_add: Option<Box<dyn Call<ReactionAdd>>>,
    pub(crate) reaction_remove: Option<Box<dyn Call<ReactionRemove>>>,
    pub(crate) reaction_remove_all: Option<Box<dyn Call<ReactionRemoveAll>>>,
    pub(crate) reaction_remove_emoji: Option<Box<dyn Call<ReactionRemoveEmoji>>>,
    pub(crate) ready: Option<Box<dyn Call<Ready>>>,
    pub(crate) resumed: Option<Box<dyn Call<()>>>,
    pub(crate) role_create: Option<Box<dyn Call<RoleCreate>>>,
    pub(crate) role_delete: Option<Box<dyn Call<RoleDelete>>>,
    pub(crate) role_update: Option<Box<dyn Call<RoleUpdate>>>,
    pub(crate) stage_instance_create: Option<Box<dyn Call<StageInstanceCreate>>>,
    pub(crate) stage_instance_delete: Option<Box<dyn Call<StageInstanceDelete>>>,
    pub(crate) stage_instance_update: Option<Box<dyn Call<StageInstanceUpdate>>>,
    pub(crate) thread_create: Option<Box<dyn Call<ThreadCreate>>>,
    pub(crate) thread_delete: Option<Box<dyn Call<ThreadDelete>>>,
    pub(crate) thread_list_sync: Option<Box<dyn Call<ThreadListSync>>>,
    pub(crate) thread_member_update: Option<Box<dyn Call<ThreadMemberUpdate>>>,
    pub(crate) thread_members_update: Option<Box<dyn Call<ThreadMembersUpdate>>>,
    pub(crate) thread_update: Option<Box<dyn Call<ThreadUpdate>>>,
    pub(crate) typing_start: Option<Box<dyn Call<TypingStart>>>,
    pub(crate) unavailable_guild: Option<Box<dyn Call<UnavailableGuild>>>,
    pub(crate) user_update: Option<Box<dyn Call<UserUpdate>>>,
    pub(crate) voice_server_update: Option<Box<dyn Call<VoiceServerUpdate>>>,
    pub(crate) voice_state_update: Option<Box<dyn Call<VoiceStateUpdate>>>,
    pub(crate) webhooks_update: Option<Box<dyn Call<WebhooksUpdate>>>,
    pub(crate) gateway_close: Option<Box<dyn Call<Option<CloseFrame<'static>>>>>,
    pub(crate) guild_audit_log_entry_create: Option<Box<dyn Call<Box<GuildAuditLogEntryCreate>>>>,
}

impl AppService {
    /// Get the registered handler for the given command name.
    fn get_cmd(&self, name: &str) -> Option<&CommandNav> {
        match self.commands.get(name) {
            Some(i) => Some(i),
            None => None,
        }
    }

    pub async fn get_component(&self, id: &Uuid) -> Option<Arc<dyn Call<Interaction>>> {
        let lock = self.ctx.component_registry.read().await;
        lock.get(id).cloned()
    }

    async fn get_component_from_str(&self, id: &str) -> Option<Arc<dyn Call<Interaction>>> {
        if id.len() < 36 {
            return None;
        }
        let id_string = &id[..36];
        let uuid = Uuid::from_str(id_string).ok()?;
        self.get_component(&uuid).await
    }

    /// Start the service and begin serving requests
    pub async fn serve(self, mut shard: Shard, wanted_event_types: EventTypeFlags) {
        let app_service = Arc::new(self);
        while let Some(event) = shard.next_event(wanted_event_types).await {
            if let Ok(event) = event {
                let srv = app_service.clone();
                let event_type = format!("{:?}", event.kind());
                let event_id = Uuid::new_v4();
                let my_span = tracing::span!(
                    tracing::Level::INFO,
                    "NEW EVENT",
                    event_type = event_type.as_str(),
                    %event_id
                );
                tokio::spawn(async move {
                    Box::pin(AppService::handle(srv, event))
                        .instrument(my_span)
                        .await;
                });
            } else {
                error!("{:?}", event);
            }
        }
    }

    /// Find and call the registered handler for the given event.
    /// When no handler is registered for the given event no further action will be taken.
    #[tracing::instrument(skip(app_service, event))]
    pub async fn handle(app_service: Arc<AppService>, event: Event) {
        match event {
            Event::InteractionCreate(i) => match i.kind {
                InteractionType::Ping => {}
                InteractionType::ApplicationCommand => {
                    if let Some(InteractionData::ApplicationCommand(c)) = &i.data {
                        if let Some(f) = app_service.get_cmd(&c.name) {
                            Box::pin(f.call(&app_service.ctx, i.0)).await;
                        }
                    }
                }
                InteractionType::ApplicationCommandAutocomplete => {
                    if let Some(InteractionData::ApplicationCommand(a)) = &i.data {
                        if let Some(f) = app_service.get_cmd(&a.name) {
                            Box::pin(f.autocomplete(&app_service.ctx, i.0)).await;
                        }
                    }
                }
                InteractionType::MessageComponent => {
                    if let Some(InteractionData::MessageComponent(data)) = &i.data {
                        if data.custom_id.len() >= 36 {
                            let id_string = &data.custom_id[..36];
                            if let Ok(id) = Uuid::from_str(id_string) {
                                if let Some(func) = app_service.get_component(&id).await {
                                    func.call(&app_service.ctx, i.0).await;
                                    return;
                                }
                                println!("fn not found");
                            }
                        }
                        if let Some(func) = &app_service.message_component_fallback {
                            func.call(&app_service.ctx, i.0).await;
                        }
                    }
                }
                InteractionType::ModalSubmit => {
                    if let Some(InteractionData::ModalSubmit(data)) = &i.data {
                        if data.custom_id.len() >= 36 {
                            let id_string = &data.custom_id[..36];
                            if let Some(func) = app_service.get_component_from_str(id_string).await
                            {
                                func.call(&app_service.ctx, i.0).await;
                                return;
                            }
                        }
                        if let Some(func) = &app_service.modal_submit_fallback {
                            func.call(&app_service.ctx, i.0).await;
                        }
                    }
                }
                _ => {}
            },
            Event::AutoModerationActionExecution(v) => {
                if let Some(func) = &app_service.auto_moderation_action_execution {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::AutoModerationRuleCreate(v) => {
                if let Some(func) = &app_service.auto_moderation_rule_create {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::AutoModerationRuleDelete(v) => {
                if let Some(func) = &app_service.auto_moderation_rule_delete {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::AutoModerationRuleUpdate(v) => {
                if let Some(func) = &app_service.auto_moderation_rule_update {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::BanAdd(v) => {
                if let Some(func) = &app_service.ban_add {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::BanRemove(v) => {
                if let Some(func) = &app_service.ban_remove {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::ChannelCreate(v) => {
                if let Some(func) = &app_service.channel_create {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::ChannelDelete(v) => {
                if let Some(func) = &app_service.channel_delete {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::ChannelPinsUpdate(v) => {
                if let Some(func) = &app_service.channel_pins_update {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::ChannelUpdate(v) => {
                if let Some(func) = &app_service.channel_update {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::CommandPermissionsUpdate(v) => {
                if let Some(func) = &app_service.command_permissions_update {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::GatewayHeartbeat(v) => {
                if let Some(func) = &app_service.gateway_heartbeat {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::GatewayHeartbeatAck => {
                if let Some(func) = &app_service.gateway_heartbeat_ack {
                    func.call(&app_service.ctx, ()).await;
                }
            }
            Event::GatewayHello(v) => {
                if let Some(func) = &app_service.gateway_hello {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::GatewayInvalidateSession(v) => {
                if let Some(func) = &app_service.gateway_invalidate_session {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::GatewayReconnect => {
                if let Some(func) = &app_service.gateway_reconnect {
                    func.call(&app_service.ctx, ()).await;
                }
            }
            Event::GuildCreate(v) => {
                if let Some(func) = &app_service.guild_create {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::GuildDelete(v) => {
                if let Some(func) = &app_service.guild_delete {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::GuildEmojisUpdate(v) => {
                if let Some(func) = &app_service.guild_emojis_update {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::GuildIntegrationsUpdate(v) => {
                if let Some(func) = &app_service.guild_integrations_update {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::GuildScheduledEventCreate(v) => {
                if let Some(func) = &app_service.guild_scheduled_event_create {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::GuildScheduledEventDelete(v) => {
                if let Some(func) = &app_service.guild_scheduled_event_delete {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::GuildScheduledEventUpdate(v) => {
                if let Some(func) = &app_service.guild_scheduled_event_update {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::GuildScheduledEventUserAdd(v) => {
                if let Some(func) = &app_service.guild_scheduled_event_user_add {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::GuildScheduledEventUserRemove(v) => {
                if let Some(func) = &app_service.guild_scheduled_event_user_remove {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::GuildStickersUpdate(v) => {
                if let Some(func) = &app_service.guild_stickers_update {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::GuildUpdate(v) => {
                if let Some(func) = &app_service.guild_update {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::IntegrationCreate(v) => {
                if let Some(func) = &app_service.integration_create {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::IntegrationDelete(v) => {
                if let Some(func) = &app_service.integration_delete {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::IntegrationUpdate(v) => {
                if let Some(func) = &app_service.integration_update {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::InviteCreate(v) => {
                if let Some(func) = &app_service.invite_create {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::InviteDelete(v) => {
                if let Some(func) = &app_service.invite_delete {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::MemberAdd(v) => {
                if let Some(func) = &app_service.member_add {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::MemberRemove(v) => {
                if let Some(func) = &app_service.member_remove {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::MemberUpdate(v) => {
                if let Some(func) = &app_service.member_update {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::MemberChunk(v) => {
                if let Some(func) = &app_service.member_chunk {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::MessageCreate(v) => {
                if let Some(func) = &app_service.message_create {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::MessageDelete(v) => {
                if let Some(func) = &app_service.message_delete {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::MessageDeleteBulk(v) => {
                if let Some(func) = &app_service.message_delete_bulk {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::MessageUpdate(v) => {
                if let Some(func) = &app_service.message_update {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::PresenceUpdate(v) => {
                if let Some(func) = &app_service.presence_update {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::ReactionAdd(v) => {
                if let Some(func) = &app_service.reaction_add {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::ReactionRemove(v) => {
                if let Some(func) = &app_service.reaction_remove {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::ReactionRemoveAll(v) => {
                if let Some(func) = &app_service.reaction_remove_all {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::ReactionRemoveEmoji(v) => {
                if let Some(func) = &app_service.reaction_remove_emoji {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::Ready(v) => {
                if let Some(func) = &app_service.ready {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::Resumed => {
                if let Some(func) = &app_service.resumed {
                    func.call(&app_service.ctx, ()).await;
                }
            }
            Event::RoleCreate(v) => {
                if let Some(func) = &app_service.role_create {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::RoleDelete(v) => {
                if let Some(func) = &app_service.role_delete {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::RoleUpdate(v) => {
                if let Some(func) = &app_service.role_update {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::StageInstanceCreate(v) => {
                if let Some(func) = &app_service.stage_instance_create {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::StageInstanceDelete(v) => {
                if let Some(func) = &app_service.stage_instance_delete {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::StageInstanceUpdate(v) => {
                if let Some(func) = &app_service.stage_instance_update {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::ThreadCreate(v) => {
                if let Some(func) = &app_service.thread_create {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::ThreadDelete(v) => {
                if let Some(func) = &app_service.thread_delete {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::ThreadListSync(v) => {
                if let Some(func) = &app_service.thread_list_sync {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::ThreadMemberUpdate(v) => {
                if let Some(func) = &app_service.thread_member_update {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::ThreadMembersUpdate(v) => {
                if let Some(func) = &app_service.thread_members_update {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::ThreadUpdate(v) => {
                if let Some(func) = &app_service.thread_update {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::TypingStart(v) => {
                if let Some(func) = &app_service.typing_start {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::UnavailableGuild(v) => {
                if let Some(func) = &app_service.unavailable_guild {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::UserUpdate(v) => {
                if let Some(func) = &app_service.user_update {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::VoiceServerUpdate(v) => {
                if let Some(func) = &app_service.voice_server_update {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::VoiceStateUpdate(v) => {
                if let Some(func) = &app_service.voice_state_update {
                    func.call(&app_service.ctx, *v).await;
                }
            }
            Event::WebhooksUpdate(v) => {
                if let Some(func) = &app_service.webhooks_update {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::GatewayClose(v) => {
                if let Some(func) = &app_service.gateway_close {
                    func.call(&app_service.ctx, v).await;
                }
            }
            Event::GuildAuditLogEntryCreate(v) => {
                if let Some(func) = &app_service.guild_audit_log_entry_create {
                    func.call(&app_service.ctx, v).await;
                }
            }
        }
    }
}
