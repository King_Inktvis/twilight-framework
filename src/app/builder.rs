use crate::app::service::AppService;
use crate::app::Context;
use crate::arguments::{Extract, Handler, StateData, StateExtract};
use crate::data::Data;
use crate::events::interaction_create::{CommandBuilder, CommandNav};
use crate::events::{Call, CallBox};
use crate::traits::Responder;
use std::any::{Any, TypeId};
use std::collections::HashMap;
use std::error::Error;
use std::sync::Arc;
use twilight_http::Client;
use twilight_model::application::command::Command;
use twilight_model::application::interaction::Interaction;
use twilight_model::gateway::payload::incoming::*;
use twilight_model::gateway::CloseFrame;
use twilight_model::id::marker::ApplicationMarker;
use twilight_model::id::Id;
use uuid::Uuid;

/// Builder to create a new service
pub struct AppBuilder {
    client: Client,
    application_id: Id<ApplicationMarker>,
    commands: Vec<CommandBuilder>,
    blocked_commands: Vec<String>,
    message_components: HashMap<Uuid, Arc<dyn Call<Interaction>>>,
    message_component_fallback: Option<Box<dyn Call<Interaction>>>,
    modal_submit_fallback: Option<Box<dyn Call<Interaction>>>,
    data: HashMap<TypeId, Arc<dyn Any + Send + Sync + 'static>>,
    auto_moderation_action_execution: Option<Box<dyn Call<AutoModerationActionExecution>>>,
    auto_moderation_rule_create: Option<Box<dyn Call<AutoModerationRuleCreate>>>,
    auto_moderation_rule_delete: Option<Box<dyn Call<AutoModerationRuleDelete>>>,
    auto_moderation_rule_update: Option<Box<dyn Call<AutoModerationRuleUpdate>>>,
    ban_add: Option<Box<dyn Call<BanAdd>>>,
    ban_remove: Option<Box<dyn Call<BanRemove>>>,
    channel_create: Option<Box<dyn Call<ChannelCreate>>>,
    channel_delete: Option<Box<dyn Call<ChannelDelete>>>,
    channel_pins_update: Option<Box<dyn Call<ChannelPinsUpdate>>>,
    channel_update: Option<Box<dyn Call<ChannelUpdate>>>,
    command_permissions_update: Option<Box<dyn Call<CommandPermissionsUpdate>>>,
    gateway_heartbeat: Option<Box<dyn Call<u64>>>,
    gateway_heartbeat_ack: Option<Box<dyn Call<()>>>,
    gateway_hello: Option<Box<dyn Call<Hello>>>,
    gateway_invalidate_session: Option<Box<dyn Call<bool>>>,
    gateway_reconnect: Option<Box<dyn Call<()>>>,
    guild_create: Option<Box<dyn Call<GuildCreate>>>,
    guild_delete: Option<Box<dyn Call<GuildDelete>>>,
    guild_emojis_update: Option<Box<dyn Call<GuildEmojisUpdate>>>,
    guild_integrations_update: Option<Box<dyn Call<GuildIntegrationsUpdate>>>,
    guild_scheduled_event_create: Option<Box<dyn Call<GuildScheduledEventCreate>>>,
    guild_scheduled_event_delete: Option<Box<dyn Call<GuildScheduledEventDelete>>>,
    guild_scheduled_event_update: Option<Box<dyn Call<GuildScheduledEventUpdate>>>,
    guild_scheduled_event_user_add: Option<Box<dyn Call<GuildScheduledEventUserAdd>>>,
    guild_scheduled_event_user_remove: Option<Box<dyn Call<GuildScheduledEventUserRemove>>>,
    guild_stickers_update: Option<Box<dyn Call<GuildStickersUpdate>>>,
    guild_update: Option<Box<dyn Call<GuildUpdate>>>,
    integration_create: Option<Box<dyn Call<IntegrationCreate>>>,
    integration_delete: Option<Box<dyn Call<IntegrationDelete>>>,
    integration_update: Option<Box<dyn Call<IntegrationUpdate>>>,
    invite_create: Option<Box<dyn Call<InviteCreate>>>,
    invite_delete: Option<Box<dyn Call<InviteDelete>>>,
    member_add: Option<Box<dyn Call<MemberAdd>>>,
    member_remove: Option<Box<dyn Call<MemberRemove>>>,
    member_update: Option<Box<dyn Call<MemberUpdate>>>,
    member_chunk: Option<Box<dyn Call<MemberChunk>>>,
    message_create: Option<Box<dyn Call<MessageCreate>>>,
    message_delete: Option<Box<dyn Call<MessageDelete>>>,
    message_delete_bulk: Option<Box<dyn Call<MessageDeleteBulk>>>,
    message_update: Option<Box<dyn Call<MessageUpdate>>>,
    presence_update: Option<Box<dyn Call<PresenceUpdate>>>,
    reaction_add: Option<Box<dyn Call<ReactionAdd>>>,
    reaction_remove: Option<Box<dyn Call<ReactionRemove>>>,
    reaction_remove_all: Option<Box<dyn Call<ReactionRemoveAll>>>,
    reaction_remove_emoji: Option<Box<dyn Call<ReactionRemoveEmoji>>>,
    ready: Option<Box<dyn Call<Ready>>>,
    resumed: Option<Box<dyn Call<()>>>,
    role_create: Option<Box<dyn Call<RoleCreate>>>,
    role_delete: Option<Box<dyn Call<RoleDelete>>>,
    role_update: Option<Box<dyn Call<RoleUpdate>>>,
    stage_instance_create: Option<Box<dyn Call<StageInstanceCreate>>>,
    stage_instance_delete: Option<Box<dyn Call<StageInstanceDelete>>>,
    stage_instance_update: Option<Box<dyn Call<StageInstanceUpdate>>>,
    thread_create: Option<Box<dyn Call<ThreadCreate>>>,
    thread_delete: Option<Box<dyn Call<ThreadDelete>>>,
    thread_list_sync: Option<Box<dyn Call<ThreadListSync>>>,
    thread_member_update: Option<Box<dyn Call<ThreadMemberUpdate>>>,
    thread_members_update: Option<Box<dyn Call<ThreadMembersUpdate>>>,
    thread_update: Option<Box<dyn Call<ThreadUpdate>>>,
    typing_start: Option<Box<dyn Call<TypingStart>>>,
    unavailable_guild: Option<Box<dyn Call<UnavailableGuild>>>,
    user_update: Option<Box<dyn Call<UserUpdate>>>,
    voice_server_update: Option<Box<dyn Call<VoiceServerUpdate>>>,
    voice_state_update: Option<Box<dyn Call<VoiceStateUpdate>>>,
    webhooks_update: Option<Box<dyn Call<WebhooksUpdate>>>,
    gateway_close: Option<Box<dyn Call<Option<CloseFrame<'static>>>>>,
    guild_audit_log_entry_create: Option<Box<dyn Call<Box<GuildAuditLogEntryCreate>>>>,
}

impl AppBuilder {
    /// Create a new appbuilder by providing the discord api token and application id.
    /// Your application can be found here <https://discord.com/developers/applications>.
    pub fn new<T: Into<String>>(token: T, application_id: u64) -> AppBuilder {
        let client = Client::new(token.into());
        let application_id = Id::new(application_id);
        AppBuilder {
            client,
            application_id,
            commands: Vec::new(),
            blocked_commands: Vec::new(),
            message_components: HashMap::default(),
            message_component_fallback: None,
            modal_submit_fallback: None,
            data: HashMap::default(),
            auto_moderation_action_execution: None,
            auto_moderation_rule_create: None,
            auto_moderation_rule_delete: None,
            auto_moderation_rule_update: None,
            ban_add: None,
            ban_remove: None,
            channel_create: None,
            channel_delete: None,
            channel_pins_update: None,
            channel_update: None,
            command_permissions_update: None,
            gateway_heartbeat: None,
            gateway_heartbeat_ack: None,
            gateway_hello: None,
            gateway_invalidate_session: None,
            gateway_reconnect: None,
            guild_create: None,
            guild_delete: None,
            guild_emojis_update: None,
            guild_integrations_update: None,
            guild_scheduled_event_create: None,
            guild_scheduled_event_delete: None,
            guild_scheduled_event_update: None,
            guild_scheduled_event_user_add: None,
            guild_scheduled_event_user_remove: None,
            guild_stickers_update: None,
            guild_update: None,
            integration_create: None,
            integration_delete: None,
            integration_update: None,
            invite_create: None,
            invite_delete: None,
            member_add: None,
            member_remove: None,
            member_update: None,
            member_chunk: None,
            message_create: None,
            message_delete: None,
            message_delete_bulk: None,
            message_update: None,
            presence_update: None,
            reaction_add: None,
            reaction_remove: None,
            reaction_remove_all: None,
            reaction_remove_emoji: None,
            ready: None,
            resumed: None,
            role_create: None,
            role_delete: None,
            role_update: None,
            stage_instance_create: None,
            stage_instance_delete: None,
            stage_instance_update: None,
            thread_create: None,
            thread_delete: None,
            thread_list_sync: None,
            thread_member_update: None,
            thread_members_update: None,
            thread_update: None,
            typing_start: None,
            unavailable_guild: None,
            user_update: None,
            voice_server_update: None,
            voice_state_update: None,
            webhooks_update: None,
            gateway_close: None,
            guild_audit_log_entry_create: None,
        }
    }

    /// Add a new command for the user to call by providing a `CommandBuilder`
    pub fn add_interaction(&mut self, route: CommandBuilder) -> &mut Self {
        self.commands.push(route);
        self
    }

    /// Block interaction with a given name from getting used during the build step
    pub fn block_interaction(&mut self, to_block: String) -> &mut Self {
        self.blocked_commands.push(to_block);
        self
    }

    /// Register a function handler for component interactions on component with the given id
    pub fn add_component<Func, Args, State>(
        &mut self,
        id: Uuid,
        state: State,
        handle: Func,
    ) -> &mut Self
    where
        State: Send + Sync + 'static,
        Func: Handler<Args>,
        Func::Output: Responder<Interaction>,
        Args: StateExtract<Interaction, State> + 'static,
    {
        let inner = Arc::new(StateData::new(state, handle));
        self.message_components.insert(id, inner);
        self
    }

    /// Add data to the global data store
    /// This data will be put in an Arc so don't do it yourself to prevent double Arc's
    pub fn add_data<U: Any + Send + Sync + 'static>(&mut self, data: U) -> &mut Self {
        self.data.insert(TypeId::of::<U>(), Arc::new(data));
        self
    }

    /// Add data to the global data store
    /// Use this instead of `add_data` if the data is already inside an Arc
    pub fn add_data_in_arc<U: Any + Send + Sync + 'static>(&mut self, data: Arc<U>) -> &mut Self {
        self.data.insert(TypeId::of::<U>(), data);
        self
    }

    /// Get data added to the builder
    #[must_use]
    pub fn get_data<T: Send + Sync + 'static>(&self) -> Option<Data<T>> {
        let tmp = self.data.get(&TypeId::of::<T>()).cloned();
        match tmp {
            Some(v) => match v.downcast::<T>() {
                Ok(v) => Some(Data(v)),
                Err(_) => None,
            },
            None => None,
        }
    }

    /// Function which will be called if the component does not contain a registered id
    pub fn message_component_fallback<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<Interaction>,
        Args: Extract<Interaction> + 'static,
    {
        self.message_component_fallback = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// Add a fallback function which will be called when a modal submit does not contain a known id
    pub fn modal_submit_fallback<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<Interaction>,
        Args: Extract<Interaction> + 'static,
    {
        self.modal_submit_fallback = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// Build the application service consuming this builder.
    /// Commands will not be registered with Discord.
    pub async fn build_no_registration(self) -> Result<AppService, Box<dyn Error + Send + Sync>> {
        self.build_inner(false).await
    }

    /// Build the application service consuming this builder.
    /// This also registers all provided command with Discord.
    /// If command exist that are not specified these will be overridden.
    pub async fn build(self) -> Result<AppService, Box<dyn Error + Send + Sync>> {
        self.build_inner(true).await
    }

    async fn build_inner(
        mut self,
        register: bool,
    ) -> Result<AppService, Box<dyn Error + Send + Sync>> {
        let interaction_client = self.client.interaction(self.application_id);
        let mut cmd_map: HashMap<String, CommandNav> = HashMap::new();
        let mut cmd_list: Vec<Command> = Vec::new();

        self.commands
            .retain(|c| !self.blocked_commands.contains(&c.name));
        self.commands
            .iter_mut()
            .for_each(|cmd| cmd.remove_from_block_list(&self.blocked_commands));

        for entry in self.commands {
            let name = entry.name.clone();
            let (cmd, cmd_nav) = entry.build();
            cmd_map.insert(name, cmd_nav);
            if register {
                cmd_list.push(cmd);
            }
        }
        if register {
            interaction_client.set_global_commands(&cmd_list).await?;
        }

        Ok(AppService {
            commands: cmd_map,
            ctx: Context {
                client: Arc::new(self.client),
                application_id: self.application_id,
                data: Arc::new(self.data),
                component_registry: Arc::default(),
            },
            message_component_fallback: self.message_component_fallback,
            modal_submit_fallback: self.modal_submit_fallback,
            auto_moderation_action_execution: self.auto_moderation_action_execution,
            auto_moderation_rule_create: self.auto_moderation_rule_create,
            auto_moderation_rule_delete: self.auto_moderation_rule_delete,
            auto_moderation_rule_update: self.auto_moderation_rule_update,
            ban_add: self.ban_add,
            ban_remove: self.ban_remove,
            channel_create: self.channel_create,
            channel_delete: self.channel_delete,
            channel_pins_update: self.channel_pins_update,
            channel_update: self.channel_update,
            command_permissions_update: self.command_permissions_update,
            gateway_heartbeat: self.gateway_heartbeat,
            gateway_heartbeat_ack: self.gateway_heartbeat_ack,
            gateway_hello: self.gateway_hello,
            gateway_invalidate_session: self.gateway_invalidate_session,
            gateway_reconnect: self.gateway_reconnect,
            guild_create: self.guild_create,
            guild_delete: self.guild_delete,
            guild_emojis_update: self.guild_emojis_update,
            guild_integrations_update: self.guild_integrations_update,
            guild_scheduled_event_create: self.guild_scheduled_event_create,
            guild_scheduled_event_delete: self.guild_scheduled_event_delete,
            guild_scheduled_event_update: self.guild_scheduled_event_update,
            guild_scheduled_event_user_add: self.guild_scheduled_event_user_add,
            guild_scheduled_event_user_remove: self.guild_scheduled_event_user_remove,
            guild_stickers_update: self.guild_stickers_update,
            guild_update: self.guild_update,
            integration_create: self.integration_create,
            integration_delete: self.integration_delete,
            integration_update: self.integration_update,
            invite_create: self.invite_create,
            invite_delete: self.invite_delete,
            member_add: self.member_add,
            member_remove: self.member_remove,
            member_update: self.member_update,
            member_chunk: self.member_chunk,
            message_create: self.message_create,
            message_delete: self.message_delete,
            message_delete_bulk: self.message_delete_bulk,
            message_update: self.message_update,
            presence_update: self.presence_update,
            reaction_add: self.reaction_add,
            reaction_remove: self.reaction_remove,
            reaction_remove_all: self.reaction_remove_all,
            reaction_remove_emoji: self.reaction_remove_emoji,
            ready: self.ready,
            resumed: self.resumed,
            role_create: self.role_create,
            role_delete: self.role_delete,
            role_update: self.role_update,
            stage_instance_create: self.stage_instance_create,
            stage_instance_delete: self.stage_instance_delete,
            stage_instance_update: self.stage_instance_update,
            thread_create: self.thread_create,
            thread_delete: self.thread_delete,
            thread_list_sync: self.thread_list_sync,
            thread_member_update: self.thread_member_update,
            thread_members_update: self.thread_members_update,
            thread_update: self.thread_update,
            typing_start: self.typing_start,
            unavailable_guild: self.unavailable_guild,
            user_update: self.user_update,
            voice_server_update: self.voice_server_update,
            voice_state_update: self.voice_state_update,
            webhooks_update: self.webhooks_update,
            gateway_close: self.gateway_close,
            guild_audit_log_entry_create: self.guild_audit_log_entry_create,
        })
    }
}

impl AppBuilder {
    /// Message was blocked by `AutoMod` according to a rule.
    pub fn auto_moderation_action_execution<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<AutoModerationActionExecution>,
        Args: Extract<AutoModerationActionExecution> + 'static,
    {
        self.auto_moderation_action_execution = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// Sent when an auto moderation rule is created.
    pub fn auto_moderation_rule_create<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<AutoModerationRuleCreate>,
        Args: Extract<AutoModerationRuleCreate> + 'static,
    {
        self.auto_moderation_rule_create = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// Sent when an auto moderation rule is deleted.
    pub fn auto_moderation_rule_delete<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<AutoModerationRuleDelete>,
        Args: Extract<AutoModerationRuleDelete> + 'static,
    {
        self.auto_moderation_rule_delete = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// Sent when an auto moderation rule is updated.
    pub fn auto_moderation_rule_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<AutoModerationRuleUpdate>,
        Args: Extract<AutoModerationRuleUpdate> + 'static,
    {
        self.auto_moderation_rule_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A user was banned from a guild.
    pub fn ban_add<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<BanAdd>,
        Args: Extract<BanAdd> + 'static,
    {
        self.ban_add = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A user's ban from a guild was removed.
    pub fn ban_remove<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<BanRemove>,
        Args: Extract<BanRemove> + 'static,
    {
        self.ban_remove = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A channel was created.
    pub fn channel_create<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<ChannelCreate>,
        Args: Extract<ChannelCreate> + 'static,
    {
        self.channel_create = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A channel was deleted.
    pub fn channel_delete<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<ChannelDelete>,
        Args: Extract<ChannelDelete> + 'static,
    {
        self.channel_delete = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A channel's pins were updated.
    pub fn channel_pins_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<ChannelPinsUpdate>,
        Args: Extract<ChannelPinsUpdate> + 'static,
    {
        self.channel_pins_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A channel was updated.
    pub fn channel_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<ChannelUpdate>,
        Args: Extract<ChannelUpdate> + 'static,
    {
        self.channel_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A command's permissions were updated.
    pub fn command_permission_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<CommandPermissionsUpdate>,
        Args: Extract<CommandPermissionsUpdate> + 'static,
    {
        self.command_permissions_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A heartbeat was sent to or received from the gateway.
    pub fn gateway_heartbeat<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<u64>,
        Args: Extract<u64> + 'static,
    {
        self.gateway_heartbeat = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A heartbeat acknowledgement was received from the gateway.
    pub fn gateway_heartbeat_ack<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<()>,
        Args: Extract<()> + 'static,
    {
        self.gateway_heartbeat_ack = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A "hello" packet was received from the gateway.
    pub fn gateway_hello<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<Hello>,
        Args: Extract<Hello> + 'static,
    {
        self.gateway_hello = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A shard's session was invalidated.
    ///
    /// `true` if resumable. If not, then the shard must do a full reconnect.
    pub fn gateway_invalidate_session<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<bool>,
        Args: Extract<bool> + 'static,
    {
        self.gateway_invalidate_session = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// The gateway is indicating to perform a reconnect.
    pub fn gateway_reconnect<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<()>,
        Args: Extract<()> + 'static,
    {
        self.gateway_reconnect = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// Undocumented event, should be ignored.
    pub fn guild_create<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<GuildCreate>,
        Args: Extract<GuildCreate> + 'static,
    {
        self.guild_create = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A guild was deleted or the current user was removed from a guild.
    pub fn guild_delete<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<GuildDelete>,
        Args: Extract<GuildDelete> + 'static,
    {
        self.guild_delete = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A guild's emojis were updated.
    pub fn guild_emojis_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<GuildEmojisUpdate>,
        Args: Extract<GuildEmojisUpdate> + 'static,
    {
        self.guild_emojis_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A guild's integrations were updated.
    pub fn guild_integrations_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<GuildIntegrationsUpdate>,
        Args: Extract<GuildIntegrationsUpdate> + 'static,
    {
        self.guild_integrations_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A guild scheduled event was created.
    pub fn guild_scheduled_event_create<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<GuildScheduledEventCreate>,
        Args: Extract<GuildScheduledEventCreate> + 'static,
    {
        self.guild_scheduled_event_create = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A guild scheduled event was deleted.
    pub fn guild_scheduled_event_delete<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<GuildScheduledEventDelete>,
        Args: Extract<GuildScheduledEventDelete> + 'static,
    {
        self.guild_scheduled_event_delete = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A guild scheduled event was updated.
    pub fn guild_scheduled_event_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<GuildScheduledEventUpdate>,
        Args: Extract<GuildScheduledEventUpdate> + 'static,
    {
        self.guild_scheduled_event_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A user was added to a guild scheduled event.
    pub fn guild_scheduled_event_user_add<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<GuildScheduledEventUserAdd>,
        Args: Extract<GuildScheduledEventUserAdd> + 'static,
    {
        self.guild_scheduled_event_user_add = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A user was removed from a guild scheduled event.
    pub fn guild_scheduled_event_user_remove<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<GuildScheduledEventUserRemove>,
        Args: Extract<GuildScheduledEventUserRemove> + 'static,
    {
        self.guild_scheduled_event_user_remove = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A guild's stickers were updated.
    pub fn guild_stickers_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<GuildStickersUpdate>,
        Args: Extract<GuildStickersUpdate> + 'static,
    {
        self.guild_stickers_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A guild's stickers were updated.
    pub fn guild_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<GuildUpdate>,
        Args: Extract<GuildUpdate> + 'static,
    {
        self.guild_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A guild integration was created.
    pub fn integration_create<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<IntegrationCreate>,
        Args: Extract<IntegrationCreate> + 'static,
    {
        self.integration_create = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A guild integration was updated.
    pub fn integration_delete<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<IntegrationDelete>,
        Args: Extract<IntegrationDelete> + 'static,
    {
        self.integration_delete = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A guild integration was deleted.
    pub fn integration_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<IntegrationUpdate>,
        Args: Extract<IntegrationUpdate> + 'static,
    {
        self.integration_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A invite was made.
    pub fn invite_create<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<InviteCreate>,
        Args: Extract<InviteCreate> + 'static,
    {
        self.invite_create = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A invite was deleted.
    pub fn invite_delete<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<InviteDelete>,
        Args: Extract<InviteDelete> + 'static,
    {
        self.invite_delete = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A user was added to a guild.
    pub fn member_add<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<MemberAdd>,
        Args: Extract<MemberAdd> + 'static,
    {
        self.member_add = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A user was removed from a guild.
    pub fn member_remove<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<MemberRemove>,
        Args: Extract<MemberRemove> + 'static,
    {
        self.member_remove = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A user's member object in a guild was updated.
    pub fn member_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<MemberUpdate>,
        Args: Extract<MemberUpdate> + 'static,
    {
        self.member_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A chunk of members were received from the gateway.
    pub fn member_chunk<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<MemberChunk>,
        Args: Extract<MemberChunk> + 'static,
    {
        self.member_chunk = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A message was created in a channel.
    pub fn message_create<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<MessageCreate>,
        Args: Extract<MessageCreate> + 'static,
    {
        self.message_create = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A message was deleted in a channel.
    pub fn message_delete<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<MessageDelete>,
        Args: Extract<MessageDelete> + 'static,
    {
        self.message_delete = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// Multiple messages were deleted in a channel.
    pub fn message_delete_bulk<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<MessageDeleteBulk>,
        Args: Extract<MessageDeleteBulk> + 'static,
    {
        self.message_delete_bulk = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A message was updated in a channel.
    pub fn message_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<MessageUpdate>,
        Args: Extract<MessageUpdate> + 'static,
    {
        self.message_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A user's active presence (such as game or online status) was updated.
    pub fn presence_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<PresenceUpdate>,
        Args: Extract<PresenceUpdate> + 'static,
    {
        self.presence_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A reaction was added to a message.
    pub fn reaction_add<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<ReactionAdd>,
        Args: Extract<ReactionAdd> + 'static,
    {
        self.reaction_add = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A reaction was removed from a message.
    pub fn reaction_remove<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<ReactionRemove>,
        Args: Extract<ReactionRemove> + 'static,
    {
        self.reaction_remove = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// All reactions were removed from a message.
    pub fn reaction_remove_all<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<ReactionRemoveAll>,
        Args: Extract<ReactionRemoveAll> + 'static,
    {
        self.reaction_remove_all = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// All instances of a given emoji from the reactions of a message were
    /// removed.
    pub fn reaction_remove_emoji<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<ReactionRemoveEmoji>,
        Args: Extract<ReactionRemoveEmoji> + 'static,
    {
        self.reaction_remove_emoji = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A shard is now "ready" and fully connected.
    pub fn ready<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<Ready>,
        Args: Extract<Ready> + 'static,
    {
        self.ready = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A shard has successfully resumed.
    pub fn resumed<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<()>,
        Args: Extract<()> + 'static,
    {
        self.resumed = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A role was created in a guild.
    pub fn role_create<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<RoleCreate>,
        Args: Extract<RoleCreate> + 'static,
    {
        self.role_create = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A role was deleted in a guild.
    pub fn role_delete<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<RoleDelete>,
        Args: Extract<RoleDelete> + 'static,
    {
        self.role_delete = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A role was updated in a guild.
    pub fn role_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<RoleUpdate>,
        Args: Extract<RoleUpdate> + 'static,
    {
        self.role_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A stage instance was created in a stage channel.
    pub fn stage_instance_create<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<StageInstanceCreate>,
        Args: Extract<StageInstanceCreate> + 'static,
    {
        self.stage_instance_create = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A stage instance was deleted in a stage channel.
    pub fn stage_instance_delete<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<StageInstanceDelete>,
        Args: Extract<StageInstanceDelete> + 'static,
    {
        self.stage_instance_delete = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A stage instance was updated in a stage channel.
    pub fn stage_instance_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<StageInstanceUpdate>,
        Args: Extract<StageInstanceUpdate> + 'static,
    {
        self.stage_instance_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A thread has been created, relevant to the current user,
    /// or the current user has been added to a thread.
    pub fn thread_create<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<ThreadCreate>,
        Args: Extract<ThreadCreate> + 'static,
    {
        self.thread_create = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A thread, relevant to the current user, has been deleted.
    pub fn thread_delete<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<ThreadDelete>,
        Args: Extract<ThreadDelete> + 'static,
    {
        self.thread_delete = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// The current user has gained access to a thread.
    pub fn thread_list_sync<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<ThreadListSync>,
        Args: Extract<ThreadListSync> + 'static,
    {
        self.thread_list_sync = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// The thread member object for the current user has been updated.
    pub fn thread_member_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<ThreadMemberUpdate>,
        Args: Extract<ThreadMemberUpdate> + 'static,
    {
        self.thread_member_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A user has been added to or removed from a thread.
    pub fn thread_members_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<ThreadMembersUpdate>,
        Args: Extract<ThreadMembersUpdate> + 'static,
    {
        self.thread_members_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A thread has been updated.
    pub fn thread_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<ThreadUpdate>,
        Args: Extract<ThreadUpdate> + 'static,
    {
        self.thread_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A user started typing in a channel.
    pub fn typing_start<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<TypingStart>,
        Args: Extract<TypingStart> + 'static,
    {
        self.typing_start = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A guild is now unavailable.
    pub fn unavailable_guild<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<UnavailableGuild>,
        Args: Extract<UnavailableGuild> + 'static,
    {
        self.unavailable_guild = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// The current user was updated.
    pub fn user_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<UserUpdate>,
        Args: Extract<UserUpdate> + 'static,
    {
        self.user_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A voice server update was sent.
    pub fn voice_server_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<VoiceServerUpdate>,
        Args: Extract<VoiceServerUpdate> + 'static,
    {
        self.voice_server_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A voice state in a voice channel was updated.
    pub fn voice_state_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<VoiceStateUpdate>,
        Args: Extract<VoiceStateUpdate> + 'static,
    {
        self.voice_state_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// A webhook was updated.
    pub fn webhooks_update<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<WebhooksUpdate>,
        Args: Extract<WebhooksUpdate> + 'static,
    {
        self.webhooks_update = Some(Box::new(CallBox::new(handle)));
        self
    }

    pub fn gateway_close<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<Option<CloseFrame<'static>>>,
        Args: Extract<Option<CloseFrame<'static>>> + 'static,
    {
        self.gateway_close = Some(Box::new(CallBox::new(handle)));
        self
    }

    pub fn guild_audit_log_entry_create<Func, Args>(&mut self, handle: Func) -> &mut Self
    where
        Func: Handler<Args>,
        Func::Output: Responder<Box<GuildAuditLogEntryCreate>>,
        Args: Extract<Box<GuildAuditLogEntryCreate>> + 'static,
    {
        self.guild_audit_log_entry_create = Some(Box::new(CallBox::new(handle)));
        self
    }
}
