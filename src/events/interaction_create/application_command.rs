use crate::app::Context;
use crate::responses::interaction_respond;
use crate::traits::Responder;
use crate::Extract;
use async_trait::async_trait;
use std::sync::Arc;
use twilight_model::application::command::CommandOption;
use twilight_model::application::interaction::application_command::{
    CommandData, CommandOptionValue,
};
use twilight_model::application::interaction::{Interaction, InteractionData};
use twilight_model::id::marker::{
    AttachmentMarker, ChannelMarker, GenericMarker, RoleMarker, UserMarker,
};
use twilight_model::id::Id;
use twilight_util::builder::command::{
    AttachmentBuilder, BooleanBuilder, ChannelBuilder, IntegerBuilder, MentionableBuilder,
    NumberBuilder, RoleBuilder, StringBuilder, UserBuilder,
};

#[async_trait]
impl Extract<Interaction> for Arc<twilight_http::Client> {
    type Error = ();

    async fn extract(ctx: &Context, _: &Interaction) -> Result<Self, Self::Error> {
        Ok(ctx.client.clone())
    }
}

#[async_trait]
impl Responder<Interaction> for String {
    async fn responder(self, ctx: &Context, result_context: &Interaction) {
        interaction_respond(ctx, result_context.id, &result_context.token, self).await;
    }
}

pub trait ToCommandOption {
    fn options() -> Vec<CommandOption>;
}

pub trait GetArg {
    type Output;
    type Error: Responder<Interaction>;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self::Output, Self::Error>;

    fn option() -> CommandOption;
}

impl GetArg for i64 {
    type Output = Self;
    type Error = String;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self, Self::Error> {
        match arg {
            Some(CommandOptionValue::Integer(int)) => Ok(*int),
            _ => Err("Wrong data type provided".into()),
        }
    }

    fn option() -> CommandOption {
        IntegerBuilder::new("", "")
            .max_value(i64::MAX)
            .min_value(i64::MIN)
            .required(true)
            .build()
    }
}

impl GetArg for u64 {
    type Output = Self;
    type Error = String;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self, Self::Error> {
        match arg {
            Some(CommandOptionValue::Integer(int)) => {
                let tmp = u64::try_from(*int).map_err(|_| "out of bounds".to_string())?;

                Ok(tmp)
            }
            _ => Err("Wrong data type provided".into()),
        }
    }

    fn option() -> CommandOption {
        IntegerBuilder::new("", "")
            .max_value(i64::MAX)
            .min_value(0)
            .required(true)
            .build()
    }
}

impl GetArg for i32 {
    type Output = Self;
    type Error = String;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self, Self::Error> {
        match arg {
            Some(CommandOptionValue::Integer(int)) => {
                let tmp = i32::try_from(*int).map_err(|_| "out of bounds".to_string())?;
                Ok(tmp)
            }
            _ => Err("Wrong data type provided".into()),
        }
    }

    fn option() -> CommandOption {
        IntegerBuilder::new("", "")
            .max_value(i32::MAX.into())
            .min_value(i32::MIN.into())
            .required(true)
            .build()
    }
}

impl GetArg for u32 {
    type Output = Self;
    type Error = String;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self, Self::Error> {
        match arg {
            Some(CommandOptionValue::Integer(int)) => {
                let tmp = u32::try_from(*int).map_err(|_| "out of bounds".to_string())?;
                Ok(tmp)
            }
            _ => Err("Wrong data type provided".into()),
        }
    }

    fn option() -> CommandOption {
        IntegerBuilder::new("", "")
            .max_value(u32::MAX.into())
            .min_value(u32::MIN.into())
            .required(true)
            .build()
    }
}

impl GetArg for i16 {
    type Output = Self;
    type Error = String;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self, Self::Error> {
        match arg {
            Some(CommandOptionValue::Integer(int)) => {
                let tmp = i16::try_from(*int).map_err(|_| "out of bounds".to_string())?;
                Ok(tmp)
            }
            _ => Err("Wrong data type provided".into()),
        }
    }

    fn option() -> CommandOption {
        IntegerBuilder::new("", "")
            .max_value(i16::MAX.into())
            .min_value(i16::MIN.into())
            .required(true)
            .build()
    }
}

impl GetArg for u16 {
    type Output = Self;
    type Error = String;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self, Self::Error> {
        match arg {
            Some(CommandOptionValue::Integer(int)) => {
                let tmp = u16::try_from(*int).map_err(|_| "out of bounds".to_string())?;
                Ok(tmp)
            }
            _ => Err("Wrong data type provided".into()),
        }
    }

    fn option() -> CommandOption {
        IntegerBuilder::new("", "")
            .max_value(u16::MAX.into())
            .min_value(u16::MIN.into())
            .required(true)
            .build()
    }
}

impl GetArg for i8 {
    type Output = Self;
    type Error = String;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self, Self::Error> {
        match arg {
            Some(CommandOptionValue::Integer(int)) => {
                let tmp = i8::try_from(*int).map_err(|_| "out of bounds".to_string())?;
                Ok(tmp)
            }
            _ => Err("Wrong data type provided".into()),
        }
    }

    fn option() -> CommandOption {
        IntegerBuilder::new("", "")
            .max_value(i8::MAX.into())
            .min_value(i8::MIN.into())
            .required(true)
            .build()
    }
}

impl GetArg for u8 {
    type Output = Self;
    type Error = String;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self, Self::Error> {
        match arg {
            Some(CommandOptionValue::Integer(int)) => {
                let tmp = u8::try_from(*int).map_err(|_| "out of bounds".to_string())?;
                Ok(tmp)
            }
            _ => Err("Wrong data type provided".into()),
        }
    }

    fn option() -> CommandOption {
        IntegerBuilder::new("", "")
            .max_value(u8::MAX.into())
            .min_value(u8::MIN.into())
            .required(true)
            .build()
    }
}

impl GetArg for f64 {
    type Output = Self;
    type Error = String;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self, Self::Error> {
        match arg {
            Some(CommandOptionValue::Number(float)) => Ok(*float),
            _ => Err("Wrong data type provided".into()),
        }
    }

    fn option() -> CommandOption {
        NumberBuilder::new("", "")
            .max_value(f64::MAX)
            .min_value(f64::MIN)
            .required(true)
            .build()
    }
}

impl GetArg for bool {
    type Output = Self;
    type Error = String;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self, Self::Error> {
        match arg {
            Some(CommandOptionValue::Boolean(b)) => Ok(*b),
            _ => Err("Wrong data type provided".into()),
        }
    }

    fn option() -> CommandOption {
        BooleanBuilder::new("", "").required(true).build()
    }
}

impl GetArg for Id<ChannelMarker> {
    type Output = Self;
    type Error = String;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self, Self::Error> {
        match arg {
            Some(CommandOptionValue::Channel(channel)) => Ok(*channel),
            _ => Err("Wrong data type provided".into()),
        }
    }

    fn option() -> CommandOption {
        ChannelBuilder::new("", "").required(true).build()
    }
}

impl GetArg for Id<GenericMarker> {
    type Output = Self;
    type Error = String;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self, Self::Error> {
        match arg {
            Some(CommandOptionValue::Mentionable(mention)) => Ok(*mention),
            _ => Err("Wrong data type provided".into()),
        }
    }

    fn option() -> CommandOption {
        MentionableBuilder::new("", "").required(true).build()
    }
}

impl GetArg for Id<RoleMarker> {
    type Output = Self;
    type Error = String;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self, Self::Error> {
        match arg {
            Some(CommandOptionValue::Role(role)) => Ok(*role),
            _ => Err("Wrong data type provided".into()),
        }
    }

    fn option() -> CommandOption {
        RoleBuilder::new("", "").required(true).build()
    }
}

impl GetArg for Id<UserMarker> {
    type Output = Self;
    type Error = String;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self, Self::Error> {
        match arg {
            Some(CommandOptionValue::User(user)) => Ok(*user),
            _ => Err("Wrong data type provided".into()),
        }
    }

    fn option() -> CommandOption {
        UserBuilder::new("", "").required(true).build()
    }
}

impl GetArg for Id<AttachmentMarker> {
    type Output = Self;
    type Error = String;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self, Self::Error> {
        match arg {
            Some(CommandOptionValue::Attachment(att)) => Ok(*att),
            _ => Err("Wrong data type provided".into()),
        }
    }

    fn option() -> CommandOption {
        AttachmentBuilder::new("", "").required(true).build()
    }
}

pub struct Range<const MIN: i64, const MAX: i64>(pub i64);

impl<const MIN: i64, const MAX: i64> GetArg for Range<MIN, MAX> {
    type Output = Self;
    type Error = String;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self, Self::Error> {
        match arg {
            Some(CommandOptionValue::Integer(int)) => {
                if *int > MAX {
                    return Err("Value is too high".into());
                }
                if *int < MIN {
                    return Err("Value is too low".into());
                }
                Ok(Range(*int))
            }
            _ => Err("Wrong data type provided".into()),
        }
    }

    fn option() -> CommandOption {
        IntegerBuilder::new("", "")
            .max_value(MAX)
            .min_value(MIN)
            .required(true)
            .build()
    }
}

impl GetArg for String {
    type Output = Self;
    type Error = String;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self, Self::Error> {
        match arg {
            Some(CommandOptionValue::String(text)) => Ok(text.clone()),
            _ => Err("Wrong data type provided".into()),
        }
    }

    fn option() -> CommandOption {
        StringBuilder::new("", "").required(true).build()
    }
}

impl<T> GetArg for Option<T>
where
    T: GetArg<Output = T>,
{
    type Output = Option<T>;
    type Error = T::Error;

    fn application_command(arg: Option<&CommandOptionValue>) -> Result<Self::Output, Self::Error> {
        match arg {
            Some(_) => match T::application_command(arg) {
                Ok(v) => Ok(Some(v)),
                Err(_) => Ok(None),
            },
            None => Ok(None),
        }
    }

    fn option() -> CommandOption {
        let mut option = T::option();
        option.required = Some(false);
        option
    }
}

#[async_trait]
impl Extract<Interaction> for CommandData {
    type Error = ();

    async fn extract(_ctx: &Context, msg: &Interaction) -> Result<Self, Self::Error> {
        if let Some(InteractionData::ApplicationCommand(data)) = &msg.data {
            Ok((**data).clone())
        } else {
            Err(())
        }
    }
}
