use crate::app::Context;
use crate::arguments::{Extract, Handler};
use crate::events::interaction_create::application_command::ToCommandOption;
use crate::events::{Call, CallBox};
use crate::traits::Responder;
pub use framework_derive as macros;
use twilight_model::application::command::{Command, CommandOption, CommandType};
use twilight_model::application::interaction::application_command::CommandOptionValue;
use twilight_model::application::interaction::{Interaction, InteractionData};
use twilight_model::id::Id;
use twilight_util::builder::command::{SubCommandBuilder, SubCommandGroupBuilder};

pub mod application_command;
mod application_command_autocomplete;
pub mod extractors;
pub mod message_component;
pub mod modal_submit;

/// Builder type for creating command and with arguments and handlers needed to respond to these commands.
/// The builder will not verify if the given arguments are compatible with Discords command limitations and therefore result in runtime errors.
#[must_use]
pub struct CommandBuilder {
    pub(crate) name: String,
    description: String,
    handler: Option<Box<dyn Call<Interaction>>>,
    sub_commands: Vec<SubCommand>,
    sub_command_groups: Vec<SubCommandGroup>,
    options: Vec<CommandOption>,
    autocomplete: Option<Box<dyn Call<Interaction>>>,
    dm_permission: Option<bool>,
    nsfw: Option<bool>,
}

impl CommandBuilder {
    pub fn new<N: Into<String>, D: Into<String>>(name: N, description: D) -> Self {
        Self {
            name: name.into(),
            description: description.into(),
            handler: None,
            sub_commands: vec![],
            sub_command_groups: vec![],
            options: vec![],
            autocomplete: None,
            dm_permission: None,
            nsfw: None,
        }
    }

    /// Remove commands found in the block list
    pub(crate) fn remove_from_block_list(&mut self, block_list: &[String]) {
        self.sub_commands
            .retain(|s| !block_list.contains(&format!("{} {}", self.name, s.name)));
        self.sub_command_groups
            .retain(|s| !block_list.contains(&format!("{} {}", self.name, s.name)));
        self.sub_command_groups
            .iter_mut()
            .for_each(|s| s.remove_sub_command(&self.name, block_list));
    }

    /// Add a handler for this command
    pub fn handler<F, Args>(mut self, handle: F) -> Self
    where
        F: Handler<Args>,
        F::Output: Responder<Interaction>,
        Args: Extract<Interaction> + 'static,
    {
        self.handler = Some(Box::new(CallBox::new(handle)));
        self
    }

    /// Add an option for this command.
    pub fn option(mut self, option: CommandOption) -> Self {
        self.options.push(option);
        self
    }

    /// Set the list of options for the command using a type implementing `ToCommandOption`
    pub fn set_options<T>(mut self) -> Self
    where
        T: ToCommandOption,
    {
        self.options = T::options();
        self
    }

    /// Indicates whether the command is available in DMs with the app, only for globally-scoped commands.
    /// By default, commands are visible.
    pub fn dm_permission(mut self, permission: bool) -> Self {
        self.dm_permission = Some(permission);
        self
    }

    /// Add a sub command
    pub fn sub_command(mut self, sub: SubCommand) -> Self {
        self.sub_commands.push(sub);
        self
    }

    /// Add a sub command group
    pub fn sub_command_group(mut self, sub: SubCommandGroup) -> Self {
        self.sub_command_groups.push(sub);
        self
    }

    /// Add an autocomplete handler
    pub fn autocomplete<Func, Args>(mut self, func: Func) -> Self
    where
        Func: Handler<Args>,
        Args: Extract<Interaction> + 'static,
        Func::Output: Responder<Interaction>,
    {
        self.autocomplete = Some(Box::new(CallBox::new(func)));
        self
    }

    pub(crate) fn build(self) -> (Command, CommandNav) {
        let mut options = vec![];
        let mut group_nav = Vec::new();
        let mut group = Vec::new();
        self.sub_command_groups
            .into_iter()
            .map(SubCommandGroup::build)
            .for_each(|(option, nav)| {
                group.push(option);
                group_nav.push(nav);
            });
        options.append(&mut group);
        let mut subs = Vec::new();
        let mut sub_nav = Vec::new();
        self.sub_commands
            .into_iter()
            .map(SubCommand::build)
            .for_each(|(option, nav)| {
                subs.push(option);
                sub_nav.push(nav);
            });
        options.append(&mut subs);
        let mut o = self.options.clone();
        options.append(&mut o);
        let command = Command {
            application_id: None,
            description: self.description.clone(),
            guild_id: None,
            id: None,
            kind: CommandType::ChatInput,
            name: self.name.clone(),
            options,
            version: Id::new(1),
            default_member_permissions: None,
            dm_permission: self.dm_permission,
            description_localizations: None,
            name_localizations: None,
            nsfw: self.nsfw,
        };
        let command_nav = CommandNav {
            handler: self.handler,
            sub_commands: sub_nav,
            sub_command_groups: group_nav,
            autocomplete: self.autocomplete,
        };
        (command, command_nav)
    }
}

/// Internal struct for storing a command post registration.
pub(crate) struct CommandNav {
    handler: Option<Box<dyn Call<Interaction>>>,
    sub_commands: Vec<SubNav>,
    sub_command_groups: Vec<SubGroupNav>,
    autocomplete: Option<Box<dyn Call<Interaction>>>,
}

/// Internal struct for storing a sub command group post registration.
struct SubGroupNav {
    name: String,
    sub_commands: Vec<SubNav>,
}

/// Internal struct for storing a sub command post registration.
struct SubNav {
    name: String,
    handler: Box<dyn Call<Interaction>>,
    autocomplete: Option<Box<dyn Call<Interaction>>>,
}

impl CommandNav {
    pub async fn call(&self, ctx: &Context, mut app_cmd: Interaction) {
        if let Some(InteractionData::ApplicationCommand(data)) = &mut app_cmd.data {
            let sub_name = data.options.iter().find_map(|option| {
                if let CommandOptionValue::SubCommand(v) = &option.value {
                    return Some((&option.name, v));
                }
                None
            });
            if let Some((sub, args)) = sub_name {
                for s in &self.sub_commands {
                    if s.name == *sub {
                        data.options = args.clone();
                        s.call(ctx, app_cmd).await;
                        return;
                    }
                }
            }
            let group_name = data.options.iter().find_map(|option| {
                if let CommandOptionValue::SubCommandGroup(v) = &option.value {
                    return Some((&option.name, v));
                }
                None
            });
            if let Some((sub_group_name, args)) = group_name {
                for sub_group in &self.sub_command_groups {
                    if sub_group.name == *sub_group_name {
                        data.options = args.clone();
                        sub_group.call(ctx, app_cmd).await;
                        return;
                    }
                }
            }
            if let Some(handle) = &self.handler {
                handle.call(ctx, app_cmd).await;
            }
        }
    }

    pub async fn autocomplete(&self, ctx: &Context, mut arg: Interaction) {
        if let Some(InteractionData::ApplicationCommand(data)) = &mut arg.data {
            let sub_name = data.options.iter().find_map(|option| {
                if let CommandOptionValue::SubCommand(v) = &option.value {
                    return Some((&option.name, v));
                }
                None
            });
            if let Some((sub, args)) = sub_name {
                for s in &self.sub_commands {
                    if s.name == *sub {
                        data.options = args.clone();
                        s.call_autocomplete(ctx, arg).await;
                        return;
                    }
                }
            }
            let group_name = data.options.iter().find_map(|option| {
                if let CommandOptionValue::SubCommandGroup(v) = &option.value {
                    return Some((&option.name, v));
                }
                None
            });
            if let Some((sub_group_name, args)) = group_name {
                for sub_group in &self.sub_command_groups {
                    if sub_group.name == *sub_group_name {
                        data.options = args.clone();
                        sub_group.call_autocomplete(ctx, arg).await;
                        return;
                    }
                }
            }
            if let Some(handle) = &self.autocomplete {
                handle.call(ctx, arg).await;
            }
        }
    }
}

impl SubGroupNav {
    async fn call(&self, ctx: &Context, mut app_cmd: Interaction) {
        if let Some(InteractionData::ApplicationCommand(data)) = &mut app_cmd.data {
            let option = data.options.iter().find_map(|option| {
                if let CommandOptionValue::SubCommand(v) = &option.value {
                    return Some((&option.name, v));
                }
                None
            });
            if let Some((name, options)) = option {
                for sub in &self.sub_commands {
                    if sub.name == *name {
                        data.options = options.clone();
                        sub.call(ctx, app_cmd).await;
                        return;
                    }
                }
            }
        }
    }

    async fn call_autocomplete(&self, ctx: &Context, mut app: Interaction) {
        if let Some(InteractionData::ApplicationCommand(data)) = &mut app.data {
            let found = data.options.iter().find_map(|option| {
                if let CommandOptionValue::SubCommand(sub) = &option.value {
                    return Some((option.name.as_str(), sub));
                }
                None
            });
            if let Some((name, options)) = found {
                for sub in &self.sub_commands {
                    if sub.name == name {
                        data.options = options.clone();
                        sub.call_autocomplete(ctx, app).await;
                        return;
                    }
                }
            }
        }
    }
}

impl SubNav {
    async fn call(&self, ctx: &Context, app_cmd: Interaction) {
        self.handler.call(ctx, app_cmd).await;
    }

    async fn call_autocomplete(&self, ctx: &Context, app: Interaction) {
        if let Some(f) = &self.autocomplete {
            f.call(ctx, app).await;
        }
    }
}

/// Builder for a subcommand
#[must_use]
pub struct SubCommand {
    name: String,
    description: String,
    options: Vec<CommandOption>,
    handle: Box<dyn Call<Interaction>>,
    autocomplete: Option<Box<dyn Call<Interaction>>>,
}

impl SubCommand {
    pub fn new<N, D, F, Args>(name: N, description: D, handle: F) -> Self
    where
        N: Into<String>,
        D: Into<String>,
        F: Handler<Args>,
        F::Output: Responder<Interaction>,
        Args: Extract<Interaction> + 'static,
    {
        Self {
            name: name.into(),
            description: description.into(),
            options: vec![],
            handle: Box::new(CallBox::new(handle)),
            autocomplete: None,
        }
    }

    /// Register an option to this subcommand.
    pub fn option(mut self, option: CommandOption) -> Self {
        self.options.push(option);
        self
    }

    /// Set the list of options for the subcommand using a type implementing `ToCommandOption`
    pub fn set_options<T>(mut self) -> Self
    where
        T: ToCommandOption,
    {
        self.options = T::options();
        self
    }

    /// Register an autocomplete handler for this subcommand.
    pub fn autocomplete<Func, Args>(mut self, func: Func) -> Self
    where
        Func: Handler<Args>,
        Args: Extract<Interaction> + 'static,
        Func::Output: Responder<Interaction>,
    {
        self.autocomplete = Some(Box::new(CallBox::new(func)));
        self
    }

    fn build(self) -> (CommandOption, SubNav) {
        let mut options =
            SubCommandBuilder::new(self.name.clone(), self.description.clone()).build();
        options.options = Some(self.options);
        let nav = SubNav {
            name: self.name,
            handler: self.handle,
            autocomplete: self.autocomplete,
        };
        (options, nav)
    }
}

/// A builder for sub command groups
#[must_use]
pub struct SubCommandGroup {
    name: String,
    description: String,
    subcommands: Vec<SubCommand>,
}

impl SubCommandGroup {
    /// Create a new sub command group builder
    pub fn new<N: Into<String>, D: Into<String>>(name: N, description: D) -> Self {
        Self {
            name: name.into(),
            description: description.into(),
            subcommands: vec![],
        }
    }

    /// Add a subcommand to this group
    pub fn sub_command(mut self, sub_command: SubCommand) -> Self {
        self.subcommands.push(sub_command);
        self
    }

    fn remove_sub_command(&mut self, name: &str, block_list: &[String]) {
        self.subcommands
            .retain(|s| !block_list.contains(&format!("{name} {} {}", self.name, s.name)));
    }

    /// Build a sub command group from the builder.
    fn build(self) -> (CommandOption, SubGroupNav) {
        let mut sub_option = Vec::new();
        let mut sub_nav = Vec::new();
        self.subcommands
            .into_iter()
            .map(SubCommand::build)
            .for_each(|(option, nav)| {
                sub_option.push(option);
                sub_nav.push(nav);
            });
        let mut option =
            SubCommandGroupBuilder::new(self.name.clone(), self.description.clone()).build();
        option.options = Some(sub_option);
        let nav = SubGroupNav {
            name: self.name,
            sub_commands: sub_nav,
        };
        (option, nav)
    }
}
