use crate::app::Context;
use crate::arguments::StateExtract;
use crate::traits::Responder;
use crate::{Call, Extract, Handler};
use async_trait::async_trait;
use std::future::Future;
use std::marker::PhantomData;
use twilight_model::application::interaction::modal::ModalInteractionData;
use twilight_model::application::interaction::{Interaction, InteractionData};
use twilight_model::channel::message::Embed;
use twilight_model::http::interaction::{InteractionResponse, InteractionResponseType};

#[async_trait]
impl Responder<Interaction> for Embed {
    async fn responder(self, ctx: &Context, data: &Interaction) {
        let interaction_client = ctx.client.interaction(ctx.application_id);
        if let Err(e) = interaction_client
            .create_response(
                data.id,
                &data.token,
                &InteractionResponse {
                    kind: InteractionResponseType::ChannelMessageWithSource,
                    data: Some(
                        twilight_util::builder::InteractionResponseDataBuilder::new()
                            .embeds(vec![self])
                            .build(),
                    ),
                },
            )
            .await
        {
            tracing::error!("{e}");
        }
    }
}

pub struct ModalData<State, Func, Args>
where
    State: Send + Sync,
    Func: Handler<Args>,
    Func::Output: Responder<Interaction>,
    Args: StateExtract<Interaction, State> + 'static,
{
    pub args: PhantomData<Args>,
    pub state: State,
    pub handle: Func,
}

#[async_trait]
impl<State, Func, Args> Call<Interaction> for ModalData<State, Func, Args>
where
    State: Send + Sync + 'static,
    Func: Handler<Args>,
    Func::Output: Responder<Interaction>,
    Args: StateExtract<Interaction, State> + 'static,
{
    async fn call(&self, ctx: &Context, interaction: Interaction) {
        match Args::modal_extract(ctx, &interaction, &self.state).await {
            Ok(extracted) => {
                let res = self.handle.call(extracted).await;
                res.responder(ctx, &interaction).await;
            }
            Err(e) => {
                e.responder(ctx, &interaction).await;
            }
        }
    }
}

pub trait ModalHandler<Args>: Clone + 'static + Send + Sync {
    type Output;
    type Future: Future<Output = Self::Output> + Send;

    fn call(&self, args: Args) -> Self::Future;
}

#[async_trait]
impl Extract<Interaction> for ModalInteractionData {
    type Error = ();

    async fn extract(_ctx: &Context, msg: &Interaction) -> Result<Self, Self::Error> {
        if let Some(InteractionData::ModalSubmit(data)) = &msg.data {
            Ok((*data).clone())
        } else {
            Err(())
        }
    }
}

#[allow(non_snake_case, unused_variables)]
pub(crate) mod macros {
    use super::*;
    use crate::responses::ErrorBox;
    use crate::responses::ResponseBoxInner;

    macro_rules! tuple_transform {( $($arg:ident)* ) => {
        #[async_trait]
        impl<T, S, $($arg,)*> StateExtract<T, S> for (S, $($arg,)*)
        where
            T: Send + Sync + 'static,
            S: Send + Sync + Clone,
            $($arg: Extract<T> + 'static,)*
        {
            type Error = ErrorBox<T>;

            async fn modal_extract(ctx: &Context, interaction: &T, state: &S) -> Result<Self,Self::Error> {
                Ok(
                    (
                        state.clone(),
                        $($arg::extract(ctx, interaction).await.map_err(|e| ErrorBox {
                        inner:Box::new(
                            ResponseBoxInner {
                                inner:Box::new(e),
                                data: Default::default(),
                            }
                        )
                    })?,)*
                    )
                )
            }
        }
    }}

    tuple_transform! {}
    tuple_transform! {A}
    tuple_transform! {A B}
    tuple_transform! {A B C}
    tuple_transform! {A B C D}
    tuple_transform! {A B C D E}
    tuple_transform! {A B C D E F}
}
