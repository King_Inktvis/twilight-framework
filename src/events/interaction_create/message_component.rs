use crate::app::{component_uuid_apply, Context};
use crate::arguments::StateExtract;
use crate::responses::send_response;
use crate::traits::Responder;
use crate::{Extract, Handler};
use async_trait::async_trait;
use std::marker::PhantomData;
use twilight_model::application::interaction::message_component::MessageComponentInteractionData;
use twilight_model::application::interaction::{Interaction, InteractionData};
use twilight_model::http::interaction::{
    InteractionResponse, InteractionResponseData, InteractionResponseType,
};
use uuid::Uuid;

pub struct ComponentId(pub Uuid);

pub struct ComponentName(pub String);

#[async_trait]
impl Extract<Interaction> for ComponentId {
    type Error = ();

    async fn extract(_ctx: &Context, msg: &Interaction) -> Result<Self, Self::Error> {
        let custom_id = match &msg.data {
            None => Err(()),
            Some(InteractionData::ApplicationCommand(_)) => Err(()),
            Some(InteractionData::MessageComponent(data)) => Ok(&data.custom_id),
            Some(InteractionData::ModalSubmit(data)) => Ok(&data.custom_id),
            Some(_) => {
                tracing::error!("unknown interaction type received");
                Err(())
            }
        }?;
        if custom_id.len() >= 36 {
            let id_string = &custom_id[..36];
            if let Ok(id) = Uuid::parse_str(id_string) {
                return Ok(ComponentId(id));
            }
        }
        Err(())
    }
}

#[async_trait]
impl Extract<Interaction> for ComponentName {
    type Error = ();

    async fn extract(_ctx: &Context, msg: &Interaction) -> Result<Self, Self::Error> {
        if let Some(InteractionData::MessageComponent(data)) = &msg.data {
            if data.custom_id.len() > 36 {
                let name = &data.custom_id[36..];
                return Ok(ComponentName(name.to_string()));
            }
        }
        Err(())
    }
}

pub struct MessageComponentData<State, Func, Args>
where
    State: Send + Sync,
    Func: Handler<Args>,
    Func::Output: Responder<Interaction>,
    Args: StateExtract<Interaction, State> + 'static,
{
    pub args: PhantomData<Args>,
    pub state: State,
    pub handle: Func,
}

#[async_trait]
impl Extract<Interaction> for MessageComponentInteractionData {
    type Error = ();

    async fn extract(_ctx: &Context, msg: &Interaction) -> Result<Self, Self::Error> {
        if let Some(InteractionData::MessageComponent(data)) = &msg.data {
            Ok(*(*data).clone())
        } else {
            Err(())
        }
    }
}

#[async_trait]
trait Register: Send + Sync + 'static {
    async fn register(self: Box<Self>, uuid: Uuid, ctx: &Context);
}

struct ResponseComponentInner<State, Func, Args>
where
    State: Send + Sync,
    Func: Handler<Args>,
    Func::Output: Responder<Interaction>,
    Args: StateExtract<Interaction, State> + 'static,
{
    args: PhantomData<Args>,
    state: State,
    handle: Func,
}

#[async_trait]
impl<State, Func, Args> Register for ResponseComponentInner<State, Func, Args>
where
    State: Send + Sync + 'static,
    Func: Handler<Args>,
    Func::Output: Responder<Interaction>,
    Args: StateExtract<Interaction, State> + 'static,
{
    async fn register(self: Box<Self>, uuid: Uuid, ctx: &Context) {
        ctx.register_component_handle(uuid, self.state, self.handle)
            .await;
    }
}

pub struct ResponseComponent {
    uuid: Uuid,
    register: Box<dyn Register>,
    response_data: InteractionResponseData,
}

impl ResponseComponent {
    pub fn new<State, Func, Args>(
        response_data: InteractionResponseData,
        state: State,
        handle: Func,
    ) -> Self
    where
        State: Send + Sync + 'static,
        Func: Handler<Args>,
        Func::Output: Responder<Interaction>,
        Args: StateExtract<Interaction, State> + 'static,
    {
        Self {
            uuid: Uuid::new_v4(),
            register: Box::new(ResponseComponentInner {
                args: PhantomData,
                state,
                handle,
            }),
            response_data,
        }
    }
}

#[async_trait]
impl Responder<Interaction> for ResponseComponent {
    async fn responder(mut self, ctx: &Context, data: &Interaction) {
        if let Some(components) = &mut self.response_data.components {
            component_uuid_apply(&self.uuid, components);
        }
        self.register.register(self.uuid, ctx).await;
        let response = InteractionResponse {
            kind: InteractionResponseType::ChannelMessageWithSource,
            data: Some(self.response_data),
        };
        send_response(ctx, data.id, data.token.as_str(), &response).await;
    }
}
