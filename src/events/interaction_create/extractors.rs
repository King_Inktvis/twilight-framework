use crate::app::Context;
use crate::arguments::Extract;
use async_trait::async_trait;
use twilight_model::application::interaction::Interaction;
use twilight_model::id::marker::{ChannelMarker, GuildMarker, UserMarker};
use twilight_model::id::Id;
use twilight_model::user::User;

#[async_trait]
impl Extract<Interaction> for Id<ChannelMarker> {
    type Error = ();

    async fn extract(_ctx: &Context, msg: &Interaction) -> Result<Self, Self::Error> {
        match msg.channel_id {
            None => Err(()),
            Some(id) => Ok(id),
        }
    }
}

#[async_trait]
impl Extract<Interaction> for Id<GuildMarker> {
    type Error = String;

    async fn extract(_ctx: &Context, msg: &Interaction) -> Result<Self, Self::Error> {
        match msg.guild_id {
            None => Err("Only available in a server".into()),
            Some(id) => Ok(id),
        }
    }
}

#[async_trait]
impl Extract<Interaction> for User {
    type Error = ();

    async fn extract(_ctx: &Context, msg: &Interaction) -> Result<Self, Self::Error> {
        match &msg.user {
            None => Err(()),
            Some(user) => Ok(user.clone()),
        }
    }
}

#[async_trait]
impl Extract<Interaction> for Id<UserMarker> {
    type Error = ();

    async fn extract(_ctx: &Context, msg: &Interaction) -> Result<Self, Self::Error> {
        match &msg.user {
            None => Err(()),
            Some(user) => Ok(user.id),
        }
    }
}
