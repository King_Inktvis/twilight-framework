pub mod interaction_create;
pub mod message_create;

use crate::app::Context;
use crate::{
    arguments::{Extract, Handler},
    traits::Responder,
};
use async_trait::async_trait;
use std::marker::PhantomData;

#[async_trait]
pub trait Call<Data>: Send + Sync {
    async fn call(&self, ctx: &Context, data: Data);
}

pub(crate) struct CallBox<Args, Data, H>
where
    H: Handler<Args>,
    Args: Extract<Data> + 'static,
    H::Output: Responder<Data>,
{
    args: PhantomData<Args>,
    data: PhantomData<Data>,
    inner: H,
}

impl<Args, Data, H> CallBox<Args, Data, H>
where
    H: Handler<Args>,
    H::Output: Responder<Data>,
    Args: Extract<Data> + 'static,
    Data: Send + Sync,
{
    pub fn new(f: H) -> Self {
        Self {
            args: PhantomData,
            data: PhantomData,
            inner: f,
        }
    }
}

#[async_trait]
impl<Args, Data, H> Call<Data> for CallBox<Args, Data, H>
where
    H: Handler<Args>,
    H::Output: Responder<Data>,
    Args: Extract<Data> + 'static,
    Data: Send + Sync,
{
    async fn call(&self, ctx: &Context, data: Data) {
        match Args::extract(ctx, &data).await {
            Ok(args) => {
                let res = self.inner.call(args).await;
                res.responder(ctx, &data).await;
            }
            Err(e) => {
                e.responder(ctx, &data).await;
            }
        }
    }
}

#[async_trait]
impl<Data> Responder<Data> for ()
where
    Data: Send + Sync,
{
    async fn responder(self, _ctx: &Context, _data: &Data) {}
}
