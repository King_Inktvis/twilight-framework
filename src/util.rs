macro_rules! call_for_event_type {
    ($arg:ident) => {
        mod $arg {
        use twilight_model::application::interaction::Interaction;
        use twilight_model::gateway::payload::incoming::invite_create::InviteCreate;
        use twilight_model::gateway::payload::incoming::reaction_remove_emoji::ReactionRemoveEmoji;
        use twilight_model::gateway::payload::incoming::*;

        $arg!(AutoModerationActionExecution);
        $arg!(AutoModerationRuleCreate);
        $arg!(AutoModerationRuleDelete);
        $arg!(AutoModerationRuleUpdate);
        $arg!(BanAdd);
        $arg!(BanRemove);
        $arg!(ChannelCreate);
        $arg!(ChannelDelete);
        $arg!(ChannelPinsUpdate);
        $arg!(ChannelUpdate);
        $arg!(CommandPermissionsUpdate);
        $arg!(GuildCreate);
        $arg!(GuildDelete);
        $arg!(GuildEmojisUpdate);
        $arg!(GuildIntegrationsUpdate);
        $arg!(GuildScheduledEventCreate);
        $arg!(GuildScheduledEventDelete);
        $arg!(GuildScheduledEventUpdate);
        $arg!(GuildScheduledEventUserAdd);
        $arg!(GuildScheduledEventUserRemove);
        $arg!(GuildStickersUpdate);
        $arg!(GuildUpdate);
        $arg!(IntegrationCreate);
        $arg!(IntegrationDelete);
        $arg!(IntegrationUpdate);
        $arg!(InteractionCreate);
        $arg!(InviteCreate);
        $arg!(InviteDelete);
        $arg!(MemberAdd);
        $arg!(MemberRemove);
        $arg!(MemberUpdate);
        $arg!(MemberChunk);
        $arg!(MessageCreate);
        $arg!(MessageDelete);
        $arg!(MessageDeleteBulk);
        $arg!(MessageUpdate);
        $arg!(PresenceUpdate);
        $arg!(ReactionAdd);
        $arg!(ReactionRemove);
        $arg!(ReactionRemoveAll);
        $arg!(ReactionRemoveEmoji);
        $arg!(Ready);
        $arg!(RoleCreate);
        $arg!(RoleDelete);
        $arg!(RoleUpdate);
        $arg!(StageInstanceCreate);
        $arg!(StageInstanceDelete);
        $arg!(StageInstanceUpdate);
        $arg!(ThreadCreate);
        $arg!(ThreadDelete);
        $arg!(ThreadListSync);
        $arg!(ThreadMemberUpdate);
        $arg!(ThreadMembersUpdate);
        $arg!(ThreadUpdate);
        $arg!(TypingStart);
        $arg!(UnavailableGuild);
        $arg!(UserUpdate);
        $arg!(VoiceServerUpdate);
        $arg!(VoiceStateUpdate);
        $arg!(WebhooksUpdate);

        $arg!(u64);
        $arg!(bool);

        $arg!(Interaction);
    }
    };
}

pub(crate) use call_for_event_type;
