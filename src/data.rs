use crate::app::Context;
use crate::arguments::FromContext;
use async_trait::async_trait;
use std::sync::Arc;

/// Wrapper type used for easy getting data from the global state.
#[derive(Debug)]
pub struct Data<T: ?Sized>(pub(crate) Arc<T>);

impl<T> Data<T> {
    pub fn new(data: T) -> Self {
        Self(Arc::new(data))
    }

    #[must_use]
    pub fn get_ref(&self) -> &T {
        &self.0
    }

    #[must_use]
    pub fn into_inner(self) -> Arc<T> {
        self.0
    }
}

#[async_trait]
impl<T, D> FromContext<D> for Data<T>
where
    T: Send + Sync + 'static,
    D: Send + Sync + 'static,
{
    type Error = ();

    async fn from_context(ctx: &Context) -> Result<Self, Self::Error> {
        ctx.data::<T>().ok_or(())
    }
}

impl<T: ?Sized> Clone for Data<T> {
    fn clone(&self) -> Self {
        Data(self.0.clone())
    }
}
