//! `twilight_framework` is a framework to more easily start creating Discord applications.
//! This crate offers an opinionated way of interaction with Discord via the [twilight](https://github.com/twilight-rs/twilight) libraries.
//! This crate is in no way affiliated with twilight.
//!
//! Another opinionated crate for interaction with Discord is [serenity](https://github.com/serenity-rs/serenity).
//!
//! The aim of the crate is to write functions who's signature makes it clear what the will do.
//! This is done by letting the user explicitly state which data to take as parameters.
//! Furthermore the user can return response object which will be called to send the needed data to Discord.
//! Note that this flow can at any moment be bypassed by the user.
//!

use crate::data::Data;
use arguments::{Extract, Handler};
use events::Call;

pub mod app;
pub mod arguments;
pub mod data;
pub mod events;
pub mod responses;
pub mod traits;
pub(crate) mod util;

pub trait CommandOption {
    fn gen_command_option(
        arg_list: &[twilight_model::application::interaction::application_command::CommandDataOption],
    ) -> twilight_model::application::command::Command;
}
