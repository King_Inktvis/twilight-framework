use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, DeriveInput, Ident, Lit, Meta, Variant};

#[proc_macro_derive(GenArgs, attributes(description, autocomplete))]
pub fn gen_args_derive(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let struct_name = &input.ident;

    match input.data {
        syn::Data::Struct(s) => match s.fields {
            syn::Fields::Named(fields) => {
                let mut named: Vec<proc_macro2::TokenStream> = Vec::new();
                let mut command_options = Vec::new();
                for field in fields.named.iter() {
                    let name = field.ident.as_ref().unwrap();
                    let t = &field.ty;
                    named.push(quote!{
                        #name: <#t> ::application_command(arg_list.iter().find(|i| i.name == stringify!(#name)).map(|v| &v.value)).map_err(|v| twilight_framework::responses::ErrorBox::new(v))?,
                    });

                    let description = get_description(field);
                    let autocomplete = get_autocomplete(field);

                    command_options.push(quote! {
                        {
                            let mut option = <#t>::option();
                            option.name = stringify!(#name).to_string();
                            option.description = stringify!(#description).to_string();
                            if let CommandOptionType::String = option.kind {
                                option.autocomplete = Some(#autocomplete);
                            }
                            option
                        },
                    });
                }

                let res = quote! {
                    #[async_trait::async_trait]
                    impl twilight_framework::arguments::Extract<twilight_model::application::interaction::Interaction> for #struct_name {
                        type Error = twilight_framework::responses::ErrorBox<twilight_model::application::interaction::Interaction>;

                        async fn extract(ctx: &twilight_framework::app::Context, cmd: &twilight_model::application::interaction::Interaction) -> Result<Self, Self::Error> {
                            use twilight_framework::events::interaction_create::application_command::GetArg;
                            use twilight_model::application::interaction::InteractionData;

                            if let Some(InteractionData::ApplicationCommand(data)) = &cmd.data{
                               let arg_list= &data.options;
                                Ok(Self {
                                    #(#named)*
                                })
                            }else{
                                Err(twilight_framework::responses::ErrorBox::new(()))
                            }
                        }
                    }

                    impl twilight_framework::events::interaction_create::application_command::ToCommandOption for #struct_name {
                        fn options()-> Vec<twilight_model::application::command::CommandOption> {
                            use twilight_framework::events::interaction_create::application_command::GetArg;
                            use twilight_model::application::command::{CommandOption, CommandOptionType};
                            vec![#(#command_options)*]
                        }
                    }
                };
                res.into()
            }
            _ => panic!("Only available on named fields"),
        },
        _ => panic!("Only available on structs"),
    }
}

#[proc_macro_derive(ArgType, attributes(rename))]
pub fn gen_args_enum_derive(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let struct_name = &input.ident;

    match input.data {
        syn::Data::Enum(data) => {
            let mut list: Vec<proc_macro2::TokenStream> = Vec::new();
            let mut choices = Vec::new();
            for i in data.variants.iter() {
                let name_info = get_name_info(i);
                if let syn::Fields::Unit = i.fields {
                    let name = &name_info.field_name;
                    let d = &name_info.display_name;
                    list.push(quote! {
                        #d => Ok(Self::#name),
                    });
                    choices.push(quote! {
                        twilight_model::application::command::CommandOptionChoice {
                            name: #d.to_string(),
                            value: twilight_model::application::command::CommandOptionChoiceValue::String(#d.to_string()),
                            name_localizations: None,
                        },
                    });
                } else {
                    panic!();
                }
            }

            let res = quote! {
                impl twilight_framework::events::interaction_create::application_command::GetArg for #struct_name {
                    type Output = Self;
                    type Error = ();

                    fn application_command(arg: Option<&twilight_model::application::interaction::application_command::CommandOptionValue>) -> Result<Self, Self::Error> {
                        use twilight_model::application::interaction::application_command::CommandOptionValue;
                        match arg {
                            Some(CommandOptionValue::String(text)) => {
                                match text.as_str() {
                                    #(#list)*
                                    _ => {
                                        tracing::warn!("Unknown value received");
                                        Err(())
                                    }
                                }
                            },
                            _ => {
                                tracing::warn!("Wrong data type provided");
                                Err(())
                            },
                        }
                    }

                    fn option() -> twilight_model::application::command::CommandOption {
                        use twilight_model::application::command::{CommandOption, CommandOptionType, CommandOptionChoice};

                        CommandOption {
                            autocomplete: Some(false),
                            channel_types: None,
                            choices: Some(vec![#(#choices)*]),
                            description: Default::default(),
                            max_value:None,
                            min_value:None,
                            name: Default::default(),
                            max_length: None,
                            min_length: None,
                            required: Some(true),
                            description_localizations: None,
                            kind: CommandOptionType::String,
                            name_localizations: None,
                            options: None,
                        }
                    }
                }
            };
            res.into()
        }
        _ => todo!(),
    }
}

struct NameInfo {
    field_name: Ident,
    display_name: String,
}

fn get_name_info(variant: &Variant) -> NameInfo {
    let rename = rename_value(variant);
    let display_name = match rename {
        Some(v) => v,
        None => variant.ident.to_string(),
    };
    NameInfo {
        field_name: variant.ident.clone(),
        display_name,
    }
}

fn rename_value(variant: &Variant) -> Option<String> {
    for attr in variant.attrs.iter() {
        let tmp = attr.parse_meta().ok()?;
        if let Meta::NameValue(value) = tmp {
            if value.path.segments.first().unwrap().ident == "rename" {
                if let Lit::Str(s) = value.lit {
                    return Some(s.value());
                }
            }
        }
    }
    None
}

fn get_description(field: &syn::Field) -> String {
    for attr in field.attrs.iter() {
        let tmp = attr.parse_meta().ok().unwrap();
        if let Meta::NameValue(value) = tmp {
            if value.path.segments.first().unwrap().ident == "description" {
                if let Lit::Str(s) = value.lit {
                    return s.value();
                }
            }
        }
    }
    field.ident.as_ref().unwrap().to_string()
}

fn get_autocomplete(field: &syn::Field) -> bool {
    for attr in field.attrs.iter() {
        let tmp = attr.parse_meta().ok().unwrap();
        if let Meta::NameValue(value) = tmp {
            if value.path.segments.first().unwrap().ident == "autocomplete" {
                if let Lit::Bool(b) = value.lit {
                    return b.value;
                }
            }
        }
    }
    false
}
